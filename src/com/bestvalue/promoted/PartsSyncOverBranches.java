package com.bestvalue.promoted;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bestvalue.dto.Parts;
import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class PartsSyncOverBranches {

	private static List<Parts> getParts(String dbfrom) {

		// Commented on 02-17-2017
		// Parts Sync Over Branches
		// Copy newly created parts from Source branch to Target branch

		// THIS PROCESS SHOULD NOT BE RUN FROM NY/GR TO EITHER CH OR MP
		// Should always run from
		// MP--> CH
		// CH--> NY or GR or MP

		String sql = "SELECT * from parts";

		System.out.println(sql);
		Connection conn = null;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		List<Parts> lqs = new ArrayList<Parts>();
		try {
			BvasConFatory bvasConFactory = new BvasConFatory(dbfrom);
			conn = bvasConFactory.getConnection();

			pstmt1 = conn.prepareStatement(sql);

			rs1 = pstmt1.executeQuery();
			while (rs1.next()) {
				Parts p = new Parts();
				p.setPartno(rs1.getString("partno"));
				p.setInterchangeno(rs1.getString("interchangeno"));
				p.setPartdescription(rs1.getString("partdescription"));

				p.setMakemodelcode(rs1.getString("makemodelcode"));
				p.setYear(rs1.getString("year"));
				p.setSubcategory(rs1.getString("subcategory"));
				p.setSupplierid(rs1.getInt("supplierid"));
				p.setOrderno(rs1.getInt("orderno"));
				p.setCostprice(rs1.getFloat("costprice"));
				p.setListprice(rs1.getFloat("listprice"));
				p.setActualprice(rs1.getFloat("actualprice"));

				p.setUnitsinstock(0);
				p.setUnitsonorder(0);
				p.setReorderlevel(0);

				p.setKeystonenumber(rs1.getString("keystonenumber"));
				p.setOemnumber(rs1.getString("oemnumber"));
				p.setLocation("");
				p.setOrdertype(rs1.getString("ordertype"));
				p.setCapa(rs1.getString("capa"));
				p.setRefpartno(rs1.getString("refpartno"));
				p.setCategory(rs1.getString("category"));
				// p.setMasterpartno(rs1.getString("masterpartno"));
				// p.setOthersidepartno(rs1.getString("othersidepartno"));
				p.setYearfrom(rs1.getInt("yearfrom"));
				p.setYearto(rs1.getInt("yearto"));

				lqs.add(p);
			}
			System.out.println(lqs.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} finally {
			if (pstmt1 != null) {
				try {
					pstmt1.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}
		return lqs;
	}

	// Commented on 02-17-2017
	// Parts Sync Over Branches
	// Copy newly created parts from Source branch to Target branch

	// THIS PROCESS SHOULD NOT BE RUN FROM NY/GR TO EITHER CH OR MP
	// Should always run from
	// MP--> CH
	// CH--> NY or GR or MP

	public static void main(String[] args) {
		processSync("MP", "CH");
		processSync("CH", "MP,GR");
		// processSync("GR","MP,CH");
		System.out.println("END OF PROCESS");

	}

	public static void processSync(String baseDB, String branchesstring) {

		String inserSQl = "insert into parts (PartNo,  InterchangeNo, PartDescription, "
				+ "MakeModelCode, Year, SubCategory, SupplierID, OrderNo, CostPrice, ListPrice, ActualPrice, "
				+ "UnitsInStock, UnitsOnOrder, ReorderLevel,"
				+ "KeystoneNumber, OEMNumber, Location, capa, yearfrom, yearto, refpartno, Category, ordertype) "
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

		String checkSQl = "SELECT COUNT(1) AS ispresent FROM parts WHERE partno =?";

		List<Parts> partslist = getParts(baseDB);

		String[] branches = branchesstring.split(",");
		for (String branch : branches) {
			System.out.println("Starting ..." + branch);
			try {
				BvasConFatory bvasConFactory = new BvasConFatory(branch);
				PreparedStatement pstmt1 = null;
				PreparedStatement pstmt2 = null;
				ResultSet rs1 = null;
				java.sql.Connection conn = bvasConFactory.getConnection();

				for (Parts parts : partslist) {
					pstmt1 = conn.prepareStatement(checkSQl);
					pstmt1.setString(1, parts.getPartno());
					rs1 = pstmt1.executeQuery();
					if (rs1.next()) {
						int count = rs1.getInt("ispresent");
						if (count == 0) {
							System.out.println("'" + parts.getPartno() + "',");
							pstmt2 = conn.prepareStatement(inserSQl);
							pstmt2.setString(1, parts.getPartno());
							pstmt2.setString(2, parts.getInterchangeno());
							pstmt2.setString(3, parts.getPartdescription());
							pstmt2.setString(4, parts.getMakemodelcode());
							pstmt2.setString(5, parts.getYear());
							pstmt2.setString(6, parts.getSubcategory());
							pstmt2.setInt(7, parts.getSupplierid());
							pstmt2.setInt(8, parts.getOrderno());
							pstmt2.setFloat(9, parts.getCostprice());
							pstmt2.setFloat(10, parts.getListprice());
							pstmt2.setFloat(11, parts.getActualprice());
							pstmt2.setInt(12, 0);// parts.getUnitsinstock()
							pstmt2.setInt(13, 0);// parts.getUnitsonorder()
							pstmt2.setInt(14, 0);// parts.getReorderlevel()
							pstmt2.setString(15, parts.getKeystonenumber());
							pstmt2.setString(16, parts.getOemnumber());
							pstmt2.setString(17, "");// parts.getLocation()
							pstmt2.setString(18, parts.getCapa());
							pstmt2.setInt(19, parts.getYearfrom());
							pstmt2.setInt(20, parts.getYearto());
							pstmt2.setString(21, parts.getRefpartno());
							pstmt2.setString(22, parts.getCategory());
							pstmt2.setString(23, parts.getOrdertype());
							pstmt2.executeUpdate();

						}
					}
				}
				System.out.println("Ending ..." + branch);
				System.out.println("___________________________________________");

				if (rs1 != null) {
					rs1.close();
				}
				if (pstmt1 != null) {
					pstmt1.close();
				}
				if (pstmt2 != null) {
					pstmt2.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				System.out.println(e.toString());
				e.printStackTrace();
			}

		}

	}
}
