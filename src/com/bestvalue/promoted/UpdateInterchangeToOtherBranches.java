package com.bestvalue.promoted;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bestvalue.dto.Parts;
import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class UpdateInterchangeToOtherBranches {
//Do it as per need
//This copies the Interchange setting from CH --> other branches NY/GR/MP

	private static List<Parts> getParts(String dbfrom) {
		String sql = "SELECT * from parts";

		System.out.println(sql);
		Connection conn = null;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		List<Parts> lqs = new ArrayList<Parts>();
		try {
			BvasConFatory bvasConFactory = new BvasConFatory(dbfrom);
			conn = bvasConFactory.getConnection();

			pstmt1 = conn.prepareStatement(sql);

			rs1 = pstmt1.executeQuery();
			while (rs1.next()) {
				Parts p = new Parts();
				p.setPartno(rs1.getString("partno").trim());
				p.setInterchangeno(rs1.getString("interchangeno").trim());
				// p.setPartdescription(rs1.getString("partdescription").trim());
				p.setMakemodelcode(rs1.getString("makemodelcode").trim());
				// p.setYear(rs1.getString("year").trim());
				// p.setSubcategory(rs1.getString("subcategory").trim());
				p.setSupplierid(rs1.getInt("supplierid"));
				p.setOrderno(rs1.getInt("orderno"));
				p.setCostprice(rs1.getFloat("costprice"));
				p.setListprice(rs1.getFloat("listprice"));
				p.setActualprice(rs1.getFloat("actualprice"));

				p.setUnitsinstock(0);
				p.setUnitsonorder(0);
				p.setReorderlevel(0);

				p.setKeystonenumber(rs1.getString("keystonenumber").trim());
				p.setOemnumber(rs1.getString("oemnumber").trim());
				p.setLocation("");
				p.setOrdertype(rs1.getString("ordertype").trim());
				// p.setCapa(rs1.getString("capa").trim());
				// p.setRefpartno(rs1.getString("refpartno").trim());
				// p.setCategory(rs1.getString("category").trim());
				// p.setMasterpartno(rs1.getString("masterpartno").trim());
				// p.setOthersidepartno(rs1.getString("othersidepartno").trim());
				p.setYearfrom(rs1.getInt("yearfrom"));
				p.setYearto(rs1.getInt("yearto"));

				lqs.add(p);
			}
			System.out.println(lqs.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} finally {
			if (pstmt1 != null) {
				try {
					pstmt1.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}
		return lqs;
	}

	public static void main(String[] args) {
		String updateSQl = "update parts set interchangeno=? where partno=?";
		// Copied from
		String baseDB = "CH";
		List<Parts> partslist = getParts(baseDB);
		String branchesstring = "GR";
		String[] branches = branchesstring.split(",");
		for (String branch : branches) {
			System.out.println("Starting ..." + branch);
			try {
				BvasConFatory bvasConFactory = new BvasConFatory(branch);
				PreparedStatement pstmt1 = null;
				java.sql.Connection conn = bvasConFactory.getConnection();

				for (Parts parts : partslist) {
					pstmt1 = conn.prepareStatement(updateSQl);
					pstmt1.setString(1, parts.getInterchangeno().trim());
					pstmt1.setString(2, parts.getPartno().trim());

					pstmt1.executeUpdate();
				}
				if (pstmt1 != null) {
					pstmt1.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				System.out.println(e.toString());
				e.printStackTrace();
			}

			System.out.println("END OF PROCESS");
		}

	}

}
