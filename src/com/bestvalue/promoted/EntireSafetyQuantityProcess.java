package com.bestvalue.promoted;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.bestvalue.dto.ReorderParts;
import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class EntireSafetyQuantityProcess {

	private static List<ReorderParts> getInterchangeParts(String branch) {
		String sql = "SELECT partno, safetyquantity " + " FROM parts where interchangeno = '' and safetyquantity > 0";

		// System.out.println(sql);
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		List<ReorderParts> lqs = new ArrayList<ReorderParts>();
		try {
			BvasConFatory bvasConFactory = new BvasConFatory(branch);
			java.sql.Connection conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery(sql);
			while (rs1.next()) {
				ReorderParts rp = new ReorderParts();
				rp.setCount(rs1.getInt("safetyquantity"));
				rp.setPartno(rs1.getString("partno"));

				lqs.add(rp);
			}
			System.out.println(lqs.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (NullPointerException e1) {
			e1.printStackTrace();
			System.out.println(e1.toString());
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		return lqs;
	}

	private static List<ReorderParts> getInterchangeReOrderParts(String dbfrom) {
		String sql = "SELECT SUM(safetyquantity) cnt, interchangeno "
				+ " FROM parts where interchangeno <> '' GROUP BY interchangeno HAVING cnt > 0";

		// System.out.println(sql);
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		List<ReorderParts> lqs = new ArrayList<ReorderParts>();
		try {
			BvasConFatory bvasConFactory = new BvasConFatory(dbfrom);
			java.sql.Connection conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery(sql);
			while (rs1.next()) {
				ReorderParts rp = new ReorderParts();
				rp.setCount(rs1.getInt("cnt"));
				rp.setPartno(rs1.getString("interchangeno"));

				lqs.add(rp);
			}
			System.out.println(lqs.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (NullPointerException e1) {
			e1.printStackTrace();
			System.out.println(e1.toString());
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		return lqs;
	}

	private static Map<String, Integer> getPartPartdesc(String branch) {
		Map<String, Integer> partsMap = new HashMap<String, Integer>();
		String sql = "SELECT  p.partno , p.safetyquantity FROM parts p where p.interchangeno = '' and p.safetyquantity > 0";
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		try {
			BvasConFatory bvasConFactory = new BvasConFatory(branch);
			java.sql.Connection conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery(sql);
			while (rs1.next()) {
				partsMap.put(rs1.getString("partno"), rs1.getInt("safetyquantity"));
			}
			System.out.println(partsMap.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (NullPointerException e1) {
			e1.printStackTrace();
			System.out.println(e1.toString());
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		System.out.println("SIZER" + partsMap.size());
		return partsMap;
	}

	private static List<ReorderParts> getReOrderParts(String dbfrom, String baseSQL, Date datefrom, Date dateto) {

		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		List<ReorderParts> lqs = new ArrayList<ReorderParts>();

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(dbfrom);
			java.sql.Connection conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(baseSQL);
			pstmt1.setDate(1, new java.sql.Date(datefrom.getTime()));
			pstmt1.setDate(2, new java.sql.Date(dateto.getTime()));
			System.out.println(baseSQL);

			rs1 = pstmt1.executeQuery();
			while (rs1.next()) {
				ReorderParts rp = new ReorderParts();
				rp.setCount(rs1.getInt("Cnt"));
				rp.setPartno(rs1.getString("PartNumber"));
				// System.out.println(dbfrom + rs1.getString("PartNumber") + ":"
				// + rs1.getString("Cnt"));
				lqs.add(rp);
			}
			System.out.println(lqs.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (NullPointerException e1) {
			e1.printStackTrace();
			System.out.println(e1.toString());
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		return lqs;
	}

	public static void main(String[] args) throws ParseException {
		System.out.println("******************Processing ReOrderProcess***********");
		String baseDB = "CH";

		// getting todays date
		Calendar currentDate = Calendar.getInstance(); // Get the current date

		Date dateNow = currentDate.getTime();

		currentDate.add(Calendar.MONTH, -12);
		Date date1yearback = currentDate.getTime();
		System.out.println("date 12 months back :=>  " + date1yearback);

		String baseReorder = "SELECT FLOOR (COUNT(1) / 12)   AS cnt, invdtls.PartNumber  FROM invoice inv, invoicedetails invdtls WHERE inv.InvoiceNumber = invdtls.InvoiceNumber"
				+ " AND  inv.OrderDate >= ?  AND inv.OrderDate  <= ? AND invdtls.quantity > 0 GROUP BY invdtls.PartNumber HAVING cnt > 0 ORDER BY cnt DESC;";

		String branchesstring = "CH,MP,NY,GR";
		String[] branches = branchesstring.split(",");
		for (String branch : branches) {

			List<ReorderParts> listofparts = getReOrderParts(branch, baseReorder, date1yearback, dateNow);
			updateRelevelOrder(listofparts, branch);

			List<ReorderParts> listofinterchangeparts = getInterchangeReOrderParts(branch);
			updateplusInterchangeRelevelOrder(listofinterchangeparts, branch);

			// applying total interchange to other parts
			Map<String, Integer> rollupMap = getPartPartdesc(branch);
			UpdateRollDownToParts(rollupMap, branch);

		}

		System.out.println("finished final processing..............");

	}

	private static void updateplusInterchangeRelevelOrder(List<ReorderParts> rplist2, String dbto) {

		BvasConFatory bvasConFactory = new BvasConFatory(dbto);
		int i = 0;

		try {
			java.sql.Connection conn = bvasConFactory.getConnection();
			PreparedStatement pstmt2 = null;
			for (ReorderParts rp : rplist2) {
				i++;
				// System.out.println(dbto + " processing for...." + i + " and
				// partno:" + rp.getPartno());
				String updtInvoiceSql = "UPDATE parts  SET " + " safetyquantity = safetyquantity + " + rp.getCount()
						+ " WHERE partno='" + rp.getPartno() + "'";
				// System.out.println(updtInvoiceSql);
				pstmt2 = conn.prepareStatement(updtInvoiceSql);
				pstmt2.executeUpdate(updtInvoiceSql);

			}

			pstmt2.close();
			conn.close();
		} catch (NullPointerException e1) {
			e1.printStackTrace();
			System.out.println(e1.toString());
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
	}

	private static void updatePlusRelevel(List<ReorderParts> rplist, String BASEDB) {
		BvasConFatory bvasConFactory = new BvasConFatory(BASEDB);
		int i = 0;

		try {
			java.sql.Connection conn = bvasConFactory.getConnection();
			PreparedStatement pstmt2 = null;
			for (ReorderParts rp : rplist) {
				i++;
				// System.out.println(BASEDB + "processing for...." + i + " and
				// partno:" + rp.getPartno());
				String updtInvoiceSql = "UPDATE parts  SET " + " safetyquantity = safetyquantity + " + rp.getCount()
						+ " WHERE partno='" + rp.getPartno() + "' ";
				// System.out.println(updtInvoiceSql);
				pstmt2 = conn.prepareStatement(updtInvoiceSql);
				pstmt2.executeUpdate(updtInvoiceSql);

			}
			System.out.println("End Process....");
			pstmt2.close();
			conn.close();
		} catch (NullPointerException e1) {
			e1.printStackTrace();
			System.out.println(e1.toString());
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

	}

	private static void updateRelevelOrder(List<ReorderParts> rplist, String dbto) {
		BvasConFatory bvasConFactory = new BvasConFatory(dbto);
		int i = 0;

		try {
			java.sql.Connection conn = bvasConFactory.getConnection();
			PreparedStatement pstmt2 = null;
			for (ReorderParts rp : rplist) {
				i++;
				// System.out.println(dbto + "processing for...." + i + " and
				// partno:" + rp.getPartno());
				String updtInvoiceSql = "UPDATE parts  SET " + " safetyquantity =" + rp.getCount() + " WHERE partno='"
						+ rp.getPartno() + "' ";
				System.out.println(updtInvoiceSql);
				pstmt2 = conn.prepareStatement(updtInvoiceSql);
				pstmt2.executeUpdate(updtInvoiceSql);

			}
			System.out.println("End Process....");
			pstmt2.close();
			conn.close();
		} catch (NullPointerException e1) {
			e1.printStackTrace();
			System.out.println(e1.toString());
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

	}

	private static void UpdateRollDownToParts(Map<String, Integer> partsMap, String branch) {
		Iterator<Entry<String, Integer>> iter = partsMap.entrySet().iterator();
		BvasConFatory bvasConFactory = new BvasConFatory(branch);
		int i = 0;
		PreparedStatement pstmt2 = null;
		try {
			java.sql.Connection conn = bvasConFactory.getConnection();
			while (iter.hasNext()) {
				System.out.println(" processing.............." + branch + i++);
				Entry<String, Integer> mEntry = iter.next();

				String updtInvoiceSql = "UPDATE parts  SET " + " safetyquantity = " + mEntry.getValue()
				// + " disabled = 1"
						+ " WHERE interchangeno='" + mEntry.getKey() + "' ";
				// System.out.println(updtInvoiceSql);
				pstmt2 = conn.prepareStatement(updtInvoiceSql);
				pstmt2.executeUpdate(updtInvoiceSql);
			}
			pstmt2.close();
			conn.close();
			System.out.println("finished processing..............");
		} catch (NullPointerException e1) {
			e1.printStackTrace();
			System.out.println(e1.toString());
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

	}

}
