package com.bestvalue.promoted;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//~--- non-JDK imports --------------------------------------------------------

import com.bestvalue.dto.PartsCostprice;
import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class CostPriceTransfer {
	private static List<PartsCostprice> getParts(String db) {
		String sql = "SELECT round(p.costprice,0) as costprice,p.partno from  parts p  WHERE  p.costprice > 0.00";

		System.out.println(sql);

		Connection conn = null;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		List<PartsCostprice> lqs = new ArrayList<PartsCostprice>();

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(db);

			conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery();

			while (rs1.next()) {
				PartsCostprice sQuantity = new PartsCostprice();

				sQuantity.setPartno(rs1.getString("PartNo"));
				sQuantity.setCostprice(rs1.getFloat("costprice"));
				System.out.println(rs1.getString("PartNo") + ":" + rs1.getString("costprice"));
				lqs.add(sQuantity);
			}

			System.out.println(lqs.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} finally {
			if (pstmt1 != null) {
				try {
					pstmt1.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}

		return lqs;
	}

	public static void main(String[] args) {

		System.out.println("Finished Processing..................");

		List<PartsCostprice> lqs2 = getParts("MP");
		updateCostPrice(lqs2, "CH");
		// updateCostPrice(lqs2, "GR");

		System.out.println("Finished second Processing..................");
	}

	private static void updateCostPrice(List<PartsCostprice> lqs, String db) {
		BvasConFatory bvasConFactory = new BvasConFatory(db);
		PreparedStatement pstmt2 = null;
		PreparedStatement pstmt3 = null;

		try {
			java.sql.Connection conn = bvasConFactory.getConnection();

			for (PartsCostprice ps : lqs) {
				String updtInvoiceSql2 = "UPDATE parts  SET " + " costprice =" + ps.getCostprice() + " WHERE partno='"
						+ ps.getPartno() + "' and costprice >=" + ps.getCostprice();

				// System.out.println(updtInvoiceSql2);
				pstmt2 = conn.prepareStatement(updtInvoiceSql2);
				pstmt2.executeUpdate(updtInvoiceSql2);

			}

			System.out.println("END");
			pstmt2.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
