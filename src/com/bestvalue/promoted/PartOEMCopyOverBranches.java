package com.bestvalue.promoted;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bestvalue.dto.Parts;
import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class PartOEMCopyOverBranches {

	private static List<Parts> getParts(String dbfrom) {
		String sql = "SELECT * from parts";

		System.out.println(sql);

		Connection conn = null;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		List<Parts> lqs = new ArrayList<Parts>();

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(dbfrom);

			conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery();

			while (rs1.next()) {
				Parts p = new Parts();

				p.setPartno(rs1.getString("partno"));
				p.setInterchangeno(rs1.getString("interchangeno"));
				p.setPartdescription(rs1.getString("partdescription"));
				p.setMakemodelcode(rs1.getString("makemodelcode"));
				p.setYear(rs1.getString("year"));
				p.setSubcategory(rs1.getString("subcategory"));
				p.setSupplierid(rs1.getInt("supplierid"));
				p.setOrderno(rs1.getInt("orderno"));
				p.setCostprice(rs1.getFloat("costprice"));
				p.setListprice(rs1.getFloat("listprice"));
				p.setActualprice(rs1.getFloat("actualprice"));
				p.setUnitsinstock(0);
				p.setUnitsonorder(0);
				p.setReorderlevel(0);

				p.setKeystonenumber(rs1.getString("keystonenumber"));
				p.setOemnumber(rs1.getString("oemnumber"));
				p.setLocation("");
				p.setOrdertype(rs1.getString("ordertype"));
				p.setCapa(rs1.getString("capa"));
				p.setRefpartno(rs1.getString("refpartno"));
				p.setCategory(rs1.getString("category"));

				p.setYearfrom(rs1.getInt("yearfrom"));
				p.setYearto(rs1.getInt("yearto"));
				lqs.add(p);
			}

			System.out.println(lqs.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} finally {
			if (pstmt1 != null) {
				try {
					pstmt1.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}

		return lqs;
	}

	public static void main(String[] args) {

		// Copying OEM number from CH keystone numbers in the parts table to other
		// branches
		// Run this process every saturday

		String baseDB = "localMP";
		List<Parts> partslist = getParts(baseDB);
		String branchesstring = "MP";
		String[] branches = branchesstring.split(",");
		String sql = "UPDATE parts SET OEMnumber=? WHERE partno=?";

		for (String branch : branches) {
			System.out.println("Starting ..." + branch);

			try {
				BvasConFatory bvasConFactory = new BvasConFatory(branch);
				PreparedStatement pstmt1 = null;
				java.sql.Connection conn = bvasConFactory.getConnection();

				for (Parts parts : partslist) {
					pstmt1 = conn.prepareStatement(sql);
					pstmt1.setString(1, parts.getOemnumber().trim());
					pstmt1.setString(2, parts.getPartno().trim());
					pstmt1.executeUpdate();
					System.out.println(pstmt1.getUpdateCount());
				}

				if (pstmt1 != null) {
					pstmt1.close();
				}

				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				System.out.println(e.toString());
				e.printStackTrace();
			}

			System.out.println("***process end****");
		}
	}

}
