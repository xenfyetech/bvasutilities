package com.bestvalue.promoted;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bestvalue.dto.PartDescription;
import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class UpdatePartlink {
	public static void main(String[] args) {
		/* Copy Part Link Description from CH to other Branches */
		// String sourceDB = "CH";//
		String sourceDB = "CH";
		List<PartDescription> los = getPartPartdesc(sourceDB);

		// String branchesstring = "NY,MR,MP,GR";
		// Changed To -->
		String branchesstring = "MP,GR";
		String[] branches = branchesstring.split(",");

		for (String branch : branches) {
			System.out.println("Starting ..." + branch);
			BvasConFatory bvasConFactory = new BvasConFatory(branch);

			PreparedStatement pstmt2 = null;

			try {
				java.sql.Connection conn = bvasConFactory.getConnection();

				for (PartDescription os : los) {
					String updtInvoiceSql = "UPDATE parts SET KEYSTONENUMBER ='" + os.getPartdescription()
							+ "' WHERE PARTNO='" + os.getPartno() + "' and KEYSTONENUMBER ='';";

					System.out.println(updtInvoiceSql);
					pstmt2 = conn.prepareStatement(updtInvoiceSql);
					pstmt2.executeUpdate(updtInvoiceSql);
				}

				pstmt2.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.out.println(e.toString());
			}
		}
	}

	private static List<PartDescription> getPartPartdesc(String sourceDB) {
		List<PartDescription> los = new ArrayList<PartDescription>();
		String sql = "SELECT partno, KEYSTONENUMBER FROM parts WHERE KEYSTONENUMBER <> ''";
		PreparedStatement pstmt1 = null;
		Integer counter = 0;
		ResultSet rs1 = null;

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(sourceDB);
			java.sql.Connection conn = bvasConFactory.getConnection();

			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery(sql);

			while (rs1.next()) {
				counter++;

				PartDescription os = new PartDescription();

				os.setPartno(rs1.getString("partno"));
				os.setPartdescription(rs1.getString("KEYSTONENUMBER"));
				los.add(os);
			}

			System.out.println(counter);

			// System.out.println(partsMap.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

		return los;
	}
}
