package com.bestvalue.promoted;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bestvalue.dto.PartsListprice;
import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class ListPriceTransfer {
	private static List<PartsListprice> getParts(String db) {
		String sql = "SELECT ceil(p.listprice) as listprice,p.partno from  parts p  WHERE p.listprice > 0";

		System.out.println(sql);

		Connection conn = null;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		List<PartsListprice> lqs = new ArrayList<PartsListprice>();

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(db);

			conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery();

			while (rs1.next()) {
				PartsListprice sQuantity = new PartsListprice();

				sQuantity.setPartno(rs1.getString("PartNo"));
				sQuantity.setListprice(rs1.getFloat("listprice"));
				System.out.println(rs1.getString("PartNo") + ":" + rs1.getString("listprice"));
				lqs.add(sQuantity);
			}

			System.out.println(lqs.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} finally {
			if (pstmt1 != null) {
				try {
					pstmt1.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}

		return lqs;
	}

	public static void main(String[] args) {
		System.out.println("Processing..................");

		List<PartsListprice> lqs = getParts("CH");
		updateListPrice(lqs, "GR");

		System.out.println("Finished Processing..................");
	}

	private static void updateListPrice(List<PartsListprice> lqs, String db) {

		BvasConFatory bvasConFactory = new BvasConFatory(db);
		PreparedStatement pstmt2 = null;

		try {
			java.sql.Connection conn = bvasConFactory.getConnection();

			for (PartsListprice ps : lqs) {
				String updtInvoiceSql = "UPDATE parts  SET " + " listprice =" + ps.getListprice() + " WHERE partno='"
						+ ps.getPartno() + "' ;";

				System.out.println(updtInvoiceSql);
				pstmt2 = conn.prepareStatement(updtInvoiceSql);
				pstmt2.executeUpdate(updtInvoiceSql);
			}

			System.out.println("END");
			pstmt2.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

	}
}
