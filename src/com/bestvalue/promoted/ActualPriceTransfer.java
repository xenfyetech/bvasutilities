package com.bestvalue.promoted;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//~--- non-JDK imports --------------------------------------------------------

import com.bestvalue.dto.PartsActualprice;
import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class ActualPriceTransfer {
	private static List<PartsActualprice> getParts(String db) {
		String sql = "SELECT p.actualprice,p.partno from  parts p   WHERE  p.actualprice > 0  ORDER BY P.PARTNO";

		Connection conn = null;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		List<PartsActualprice> lqs = new ArrayList<PartsActualprice>();

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(db);

			conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery();

			while (rs1.next()) {
				PartsActualprice sQuantity = new PartsActualprice();

				sQuantity.setPartno(rs1.getString("PartNo"));
				sQuantity.setActualprice(rs1.getFloat("actualprice"));
				System.out.println(rs1.getString("PartNo") + ":" + rs1.getString("actualprice"));
				lqs.add(sQuantity);
			}

			System.out.println(lqs.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} finally {
			if (pstmt1 != null) {
				try {
					pstmt1.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}

		return lqs;
	}

	public static void main(String[] args) {
		System.out.println("Processing..................");

		List<PartsActualprice> lqs = getParts("localMP");

	
		updateActualPrice(lqs, "MP");

		System.out.println("Finished Processing..................");
	}

	private static void updateActualPrice(List<PartsActualprice> lqs, String db) {
		BvasConFatory bvasConFactory = new BvasConFatory(db);
		PreparedStatement pstmt2 = null;
		PreparedStatement pstmt3 = null;

		try {
			java.sql.Connection conn = bvasConFactory.getConnection();

			for (PartsActualprice ps : lqs) {
				String updtInvoiceSql = "UPDATE parts  SET " + " actualprice =" + ps.getActualprice()
						+ " WHERE partno='" + ps.getPartno() + "'; ";

				System.out.println(updtInvoiceSql);
				pstmt2 = conn.prepareStatement(updtInvoiceSql);
				pstmt2.executeUpdate(updtInvoiceSql);

			}

			System.out.println("END");
			pstmt2.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
