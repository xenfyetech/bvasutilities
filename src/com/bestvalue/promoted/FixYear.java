package com.bestvalue.promoted;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//~--- non-JDK imports --------------------------------------------------------

import com.bestvalue.dto.PartYear;
import com.bestvalue.ultimateutils.utils.BvasConFatory;
import com.bestvalue.ultimateutils.utils.NumberUtils;

public class FixYear {
	private static List<PartYear> getParts(String db) {
		String sql = "SELECT partno, year , partdescription , yearfrom , yearto from parts where ordertype <> 'S' and length(partdescription) > 1 and partdescription NOT LIKE 'Z%' order by partno";

		// System.out.println(sql);
		Connection conn = null;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		List<PartYear> lqs = new ArrayList<PartYear>();

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(db);

			conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery();

			while (rs1.next()) {
				PartYear rp = new PartYear();

				rp.setPartno(rs1.getString("partno"));
				rp.setYear(rs1.getString("year"));
				rp.setPartdescription(rs1.getString("partdescription"));
				rp.setYearfrom(rs1.getInt("yearfrom"));
				rp.setYearto(rs1.getInt("yearto"));
				lqs.add(rp);

				// System.out.println(rs1.getString("partno") + "::" +
				// rs1.getString("year"));
			}

			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} finally {
			if (pstmt1 != null) {
				try {
					pstmt1.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}

		return lqs;
	}
	// Change the target database to CH , GR,MP,NY

	public static void main(String[] args) {
		String targetDB = "MP";
		List<PartYear> reparts = getParts(targetDB);
		upDateYear(targetDB, reparts);
		System.out.println("printing");
		findtheOddOnes(reparts);
		System.out.println("all done");
	}

	private static void findtheOddOnes(List<PartYear> reparts) {
		int h = 0;
		// String searchsTR = "";
		// String searchsTR = "- (";
		for (PartYear partyear : reparts) {
			if (partyear.getPartdescription().contains(partyear.getYear())) {

				// System.out.println("'" + partyear.getPartno() + "',");
				// searchsTR = "(" + partyear.getYear() + ")";

				// System.out.println("'" + partyear.getPartno() + "',");

				if (!partyear.getPartdescription().equalsIgnoreCase("")) {
					if (partyear.getPartdescription().contains(partyear.getYear())) {
						int len = partyear.getPartdescription().length();
						// System.out.println("'" + partyear.getPartno() +
						// "',");

						int i = partyear.getPartdescription().indexOf(partyear.getYear());
						if (len > i) {
							String removeString = partyear.getPartdescription().substring(i, len);
							// System.out.println(partyear.getPartno() + "##" +
							// partyear.getPartdescription() + "##" +
							// partyear.getYear() + "##" + removeString);

							// String newdesc =
							// partyear.getPartdescription().replace(removeString,
							// "");
							// System.out.println(partyear.getPartno() + " " +
							// newdesc.trim() );
						}
					}
					//
				}

			} else {
				// System.out.println(h++);
				System.out.println("'" + partyear.getPartno() + "',");
				/*
				 * if (!partyear.getPartdescription().equalsIgnoreCase("")) { if
				 * (partyear.getPartdescription().contains(searchsTR)) { int len =
				 * partyear.getPartdescription().length();
				 * 
				 * 
				 * int i = partyear.getPartdescription().indexOf(searchsTR); if (len > i) {
				 * String removeString = partyear.getPartdescription().substring(i, len); //
				 * System.out.println(partyear.getPartno() + "##" + //
				 * partyear.getPartdescription() + "##" + // partyear.getYear() + "##" +
				 * removeString);
				 * 
				 * String newdesc = partyear.getPartdescription().replace(removeString, "");
				 * //System.out.println(partyear.getPartno() + "	" + newdesc + "- (" +
				 * partyear.getYear() + ")"); } } // }//if
				 */
			}

		}

	}

	private static void upDateYear(String targetDB, List<PartYear> reparts) {

		String sqlQuery = "UPDATE parts SET yearfrom =  ?, yearto =  ? , YEAR =? WHERE PARTNO= ?";
		Connection conn = null;
		PreparedStatement pstmt1 = null;
		BvasConFatory bvasConFactory = new BvasConFatory(targetDB);
		Integer yearfrom = 0;
		Integer yearto = 0;
		String curPartno = "";

		try {
			conn = bvasConFactory.getConnection();

			for (PartYear rp : reparts) {
				curPartno = rp.getPartno();

				if (rp.getYear().contains("-")) {
					String[] years = rp.getYear().split("-");

					if ((years[0].length() != 2) || (years[1].length() != 2)) {
						// System.out.println("'" + rp.getPartno() + "',");
					} else {

						// System.out.println(rp.getYear() + ":" +
						// NumberUtils.getYearFromString(years[0]) + ":"
						// + NumberUtils.getYearFromString(years[1]) );
						pstmt1 = conn.prepareStatement(sqlQuery);
						pstmt1.setInt(1, Integer.parseInt(NumberUtils.getYearFromString(years[0])));
						pstmt1.setInt(2, Integer.parseInt(NumberUtils.getYearFromString(years[1])));
						pstmt1.setString(3, rp.getYear());
						pstmt1.setString(4, rp.getPartno());
						pstmt1.execute();
					}
				} else {
					// System.out.println("'" + rp.getPartno() + "',");
				}
			}

			// rs1.close();
			pstmt1.close();
			conn.close();
		} catch (ArrayIndexOutOfBoundsException ae) {
			System.out.println("'" + curPartno + "',");
			System.out.println(ae.toString());
			ae.printStackTrace();
		} catch (SQLException e) {
			System.out.println("'" + curPartno + "',");
			System.out.println(e.toString());
			e.printStackTrace();
		}
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
