package com.bestvalue.promoted;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bestvalue.dto.PartLocation;
import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class VendorPriceAnalysis {
	/* Once in the Week only for CH Database */

	private static List<String> getParts(String db) {
		List<String> los = new ArrayList<String>();
		String sql = "SELECT p.partno FROM bvasdb.parts p WHERE p.interchangeno = ''";
		PreparedStatement pstmt1 = null;
		Integer counter = 0;
		ResultSet rs1 = null;
		try {
			BvasConFatory bvasConFactory = new BvasConFatory(db);
			java.sql.Connection conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery(sql);
			while (rs1.next()) {
				counter++;
				PartLocation os = new PartLocation();
				los.add(rs1.getString("partno"));
			}
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

		return los;
	}

	public static void main(String[] args) {
		String updateSql = "";
		String targetDB = "CH";
		PreparedStatement pstmt1 = null;
		PreparedStatement pstmt2 = null;
		ResultSet rs1 = null;
		String priceSQL = "SELECT distinct v.shortcode, vi.SellingRate FROM vendoritems vi , vendors v WHERE vi.SupplierID = v.SupplierID AND partno = ? AND VI.SellingRate > 0.01 and v.companytype NOT IN ( 'H', 'Z') ORDER BY VI.SellingRate";
		/* getting the list of parts for the order */
		List<String> los = getParts(targetDB);
		try {
			BvasConFatory bvasConFactory = new BvasConFatory(targetDB);
			java.sql.Connection conn = bvasConFactory.getConnection();
			for (String partno : los) {
				System.out.println("-------------" + partno + "-----------");
				int i = 1;
				pstmt1 = conn.prepareStatement(priceSQL);
				pstmt1.setString(1, partno);
				rs1 = pstmt1.executeQuery();
				while (rs1.next()) {
					System.out.println(rs1.getString("shortcode") + "		" + rs1.getFloat("SellingRate"));
					if (i > 5) {
						break;
					}
					if (i == 1) {
						updateSql = "UPDATE vendorpriceanalysis SET vendor1price=? , vendor1shortcode=? WHERE partno=?";
					}
					if (i == 2) {
						updateSql = "UPDATE vendorpriceanalysis SET vendor2price=? , vendor2shortcode=? WHERE partno=?";
					}
					if (i == 3) {
						updateSql = "UPDATE vendorpriceanalysis SET vendor3price=? , vendor3shortcode=? WHERE partno=?";
					}
					if (i == 4) {
						updateSql = "UPDATE vendorpriceanalysis SET vendor4price=? , vendor4shortcode=? WHERE partno=?";
					}
					if (i == 5) {
						updateSql = "UPDATE vendorpriceanalysis SET vendor5price=? , vendor5shortcode=? WHERE partno=?";
					}
					pstmt2 = conn.prepareStatement(updateSql);
					pstmt2.setFloat(1, rs1.getFloat("SellingRate"));
					pstmt2.setString(2, rs1.getString("shortcode"));
					pstmt2.setString(3, partno);
					pstmt2.executeUpdate();
					i++;
				}
				rs1.close();
			}
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
