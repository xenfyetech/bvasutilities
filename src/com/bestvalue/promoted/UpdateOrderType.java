package com.bestvalue.promoted;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bestvalue.dto.PartLocation;
import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class UpdateOrderType {

	/*
	 * getting the list of parts for the order from CH to other Branches for the
	 * Field OrderType in the Parts Table Every Saturday @ 2:00 PM
	 */

	private static List<PartLocation> getPartPartdesc(String db) {
		List<PartLocation> los = new ArrayList<PartLocation>();
		String sql = "SELECT partno, ordertype FROM parts";
		PreparedStatement pstmt1 = null;
		Integer counter = 0;
		ResultSet rs1 = null;
		try {
			BvasConFatory bvasConFactory = new BvasConFatory(db);
			java.sql.Connection conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery(sql);
			while (rs1.next()) {
				counter++;
				PartLocation os = new PartLocation();
				os.setPartno(rs1.getString("partno"));
				os.setLocation(rs1.getString("ordertype"));
				los.add(os);
			}
			System.out.println(counter);
			// System.out.println(partsMap.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

		return los;
	}

	public static void main(String[] args) {
		/* getting the list of parts for the order */
		List<PartLocation> los = getPartPartdesc("CH");

		// String branchesstring = "NY,AM,MR,MP,GR";//
		// Down AM and MR
		String branchesstring = "MP,GR";
		String[] branches = branchesstring.split(",");

		for (String branch : branches) {
			System.out.println("Starting ..." + branch);
			BvasConFatory bvasConFactory = new BvasConFatory(branch);

			PreparedStatement pstmt2 = null;
			try {
				java.sql.Connection conn = bvasConFactory.getConnection();
				for (PartLocation os : los) {

					String updtInvoiceSql = "UPDATE parts" + " SET ORDERTYPE ='" + os.getLocation() + "' WHERE PARTNO='"
							+ os.getPartno() + "'";

					// System.out.println(updtInvoiceSql);
					pstmt2 = conn.prepareStatement(updtInvoiceSql);
					pstmt2.executeUpdate(updtInvoiceSql);
				}
				pstmt2.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.out.println(e.toString());
			}

		}
		System.out.println("ALL DONE");
	}

}
