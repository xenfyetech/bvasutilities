package com.bestvalue.routine;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bestvalue.dto.CustomerName;
import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class CustomerCompare {
	private static List<CustomerName> getCustomers(String tagetDB) {

		List<CustomerName> los = new ArrayList<CustomerName>();
		String sql = "SELECT customerid, companyname FROM customer";
		PreparedStatement pstmt1 = null;
		Integer counter = 0;
		ResultSet rs1 = null;
		try {
			BvasConFatory bvasConFactory = new BvasConFatory(tagetDB);
			java.sql.Connection conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery(sql);
			while (rs1.next()) {
				counter++;
				CustomerName os = new CustomerName();
				os.setCustomerid(rs1.getString("customerid"));
				os.setCustomername(rs1.getString("companyname"));
				los.add(os);
			}
			System.out.println(counter);
			// System.out.println(partsMap.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

		return los;

	}

	public static void main(String[] args) {
		System.out.println("******************processing compare customers***********");
		List<CustomerName> customers = getCustomers("local");
		try {
			BvasConFatory bvasConFactory = new BvasConFatory("CH");
			java.sql.Connection conn = bvasConFactory.getConnection();
			PreparedStatement pstmt = null;
			ResultSet rs1 = null;
			String searchcustomerSQL = "select companyname from customer where customerid=?";
			for (CustomerName customer : customers) {
				pstmt = conn.prepareStatement(searchcustomerSQL);
				pstmt.setString(1, customer.getCustomerid());
				rs1 = pstmt.executeQuery();
				if (rs1.next()) {
					if (customer.getCustomername().equalsIgnoreCase(rs1.getString("companyname"))) {

					} else {
						System.out.println(customer.getCustomerid() + " " + rs1.getString("companyname") + "---------"
								+ customer.getCustomername());
					}
				}
			}
			rs1.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

		System.out.println("******************ends processing***********");
	}

}
