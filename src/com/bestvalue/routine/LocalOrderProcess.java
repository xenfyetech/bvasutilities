package com.bestvalue.routine;

//~--- JDK imports ------------------------------------------------------------

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//~--- non-JDK imports --------------------------------------------------------

import com.bestvalue.dto.LocalPriceCheck;
import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class LocalOrderProcess {
	private static List<LocalPriceCheck> getPartsFromMainOrder(String targetDB, String orders) {
		List<LocalPriceCheck> partsList = new ArrayList<LocalPriceCheck>();
		String sql = "SELECT distinct partno, vendorpartno,quantity  FROM vendorordereditems  WHERE orderno IN "
				+ orders + " and  vendorpartno <> '' and  vendorpartno is not null ORDER BY partno";
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(targetDB);
			java.sql.Connection conn = bvasConFactory.getConnection();

			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery(sql);

			while (rs1.next()) {
				LocalPriceCheck lPriceCheck = new LocalPriceCheck();

				lPriceCheck.setPartno(rs1.getString("partno"));
				lPriceCheck.setVendorpartno(rs1.getString("vendorpartno"));
				lPriceCheck.setQuantity(rs1.getInt("quantity"));
				partsList.add(lPriceCheck);
			}

			System.out.println(partsList.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

		return partsList;
	}

	private static void insertTempTable(List<LocalPriceCheck> lqs2, String targetDB) {
		BvasConFatory bvasConFactory = new BvasConFatory(targetDB);
		PreparedStatement pstmt0 = null;
		PreparedStatement pstmt1 = null;
		String sqlTruncate = "truncate table localorderstemp_csv;";

		try {
			java.sql.Connection conn = bvasConFactory.getConnection();

			pstmt0 = conn.prepareStatement(sqlTruncate);
			pstmt0.executeUpdate();

			for (LocalPriceCheck lPriceCheck : lqs2) {
				String sql = "insert into localorderstemp_csv (PartNo, VendorPartno, Price, Quanity,Supplierid) values("
						+ "'" + lPriceCheck.getPartno() + "', " + "'" + lPriceCheck.getVendorpartno() + "', " + " "
						+ lPriceCheck.getSellingprice() + " , " + lPriceCheck.getQuantity() + ", "
						+ lPriceCheck.getSupplierid() + ")";

				pstmt1 = conn.prepareStatement(sql);
				pstmt1.executeUpdate(sql);
			}

			pstmt0.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
	}

	public static void main(String[] args) {
		System.out.println("start");

		String orders = "(3000077,3000078,3000079)";
		String targetDB = "GR";
		List<LocalPriceCheck> lqs = getPartsFromMainOrder(targetDB, orders);
		List<LocalPriceCheck> lqs2 = updateSellingPrices(lqs, targetDB);

		insertTempTable(lqs2, targetDB);
		System.out.println("finish");
	}

	private static List<LocalPriceCheck> updateSellingPrices(List<LocalPriceCheck> lqs, String targetDB) {
		BvasConFatory bvasConFactory = new BvasConFatory(targetDB);
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;

		try {
			java.sql.Connection conn = bvasConFactory.getConnection();

			for (LocalPriceCheck lPriceCheck : lqs) {
				String sql = "SELECT supplierid, sellingrate from vendoritems where partno = '"
						+ lPriceCheck.getPartno() + "'" + " AND vendorpartno='" + lPriceCheck.getVendorpartno()
						+ "' AND supplierid IN(4,10,34) ORDER BY SELLINGRATE LIMIT 1";

				pstmt1 = conn.prepareStatement(sql);
				rs1 = pstmt1.executeQuery(sql);

				while (rs1.next()) {
					lPriceCheck.setSupplierid(rs1.getInt("supplierid"));
					lPriceCheck.setSellingprice(rs1.getFloat("sellingrate"));
				}
			}

			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

		return lqs;
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
