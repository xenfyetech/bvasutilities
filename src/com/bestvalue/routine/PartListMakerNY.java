package com.bestvalue.routine;

//~--- JDK imports ------------------------------------------------------------

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

//~--- non-JDK imports --------------------------------------------------------

import com.bestvalue.dto.StockContainer;
import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class PartListMakerNY {
	static HashSet<String> setMain = new HashSet<String>();

	private static void createPartList(Map<String, StockContainer> stockcontainerMap, String baseDB, String partlist) {
		Integer i = 0;
		Iterator<Entry<String, StockContainer>> iter = stockcontainerMap.entrySet().iterator();
		String insertsql = "insert into vendorordereditems (OrderNo, PartNo, VendorPartNo, Quantity, NoOrder, Price) values(?,?,?,?,?,?)";

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(baseDB);
			Connection conn = bvasConFactory.getConnection();
			PreparedStatement ps = conn.prepareStatement(baseDB);

			while (iter.hasNext()) {
				Entry<String, StockContainer> mEntry = iter.next();
				StockContainer stockcontainer = mEntry.getValue();

				if (setMain.add(stockcontainer.getPartno())) {
					if (stockcontainer.getRequired() > 0) {
						ps = conn.prepareStatement(insertsql);
						ps.setInt(1, Integer.parseInt(partlist));
						ps.setString(2, stockcontainer.getPartno());
						ps.setString(3, "");
						ps.setInt(4, stockcontainer.getRequired());
						ps.setInt(5, i++);
						ps.setFloat(6, 0.00F);
						ps.executeUpdate();
					}
				}
			}

			ps.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		}
	}

	private static void deleteOldListParts(String partslist, String baseDB) {
		String sqlOrder = "select PartNo from  vendorordereditems where orderno=?";
		String deletepartssql = "delete from vendorordereditems WHERE  orderno = ? and partno = ?";
		BvasConFatory bvasConFactoryBase = new BvasConFatory(baseDB);

		Integer onless = Integer.parseInt(partslist.trim()) - 1;
		if (onless != null) {
			if (onless > 0) {
				try {
					Connection connBase = bvasConFactoryBase.getConnection();
					PreparedStatement psBase = connBase.prepareStatement(sqlOrder);
					psBase.setInt(1, onless);
					ResultSet rsBase = psBase.executeQuery();
					System.out.println(onless);

					PreparedStatement statmt1Target = connBase.prepareStatement(deletepartssql);

					while (rsBase.next()) {
						String partno = rsBase.getString("PartNo");
						statmt1Target.setInt(1, Integer.parseInt(partslist.trim())); // set
						statmt1Target.setString(2, partno.trim()); // set
						statmt1Target.executeUpdate();
					}

					statmt1Target.close();
					rsBase.close();
					psBase.close();
					connBase.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}
	}

	private static Map<String, StockContainer> getStockContainerFromBase(String baseDB, String container) {
		Map<String, StockContainer> stockcontainerMap = new HashMap<String, StockContainer>();
		String scSQL = "SELECT p.partno , p.unitsinstock, v.quantity " + " FROM parts p ,  vendorordereditems v "
				+ " WHERE p.partno  = v.partno " + " AND v.OrderNo =" + Integer.parseInt(container.trim())
				+ " ORDER BY p.partno, v.quantity desc";

		System.out.println(scSQL);

		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;

		try {

			// checking duplicate
			HashSet<String> set = new HashSet<String>();
			BvasConFatory bvasConFactory = new BvasConFatory(baseDB);
			java.sql.Connection conn = bvasConFactory.getConnection();

			pstmt1 = conn.prepareStatement(scSQL);
			rs1 = pstmt1.executeQuery(scSQL);

			while (rs1.next()) {
				StockContainer sc = new StockContainer();

				sc.setRequired(0); // to be filled from target db
				sc.setPartno(rs1.getString("partno"));
				sc.setUnitsinstock(rs1.getInt("unitsinstock"));
				sc.setContainerquantity(rs1.getInt("quantity"));

				if (set.add(rs1.getString("partno"))) {
					stockcontainerMap.put(rs1.getString("partno"), sc);
				}
			}

			System.out.println(stockcontainerMap.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

		return stockcontainerMap;
	}

	public static void main(String[] args) {
		System.out.println("******************Processing Partlist***********");

		String baseDB = "CH";
		Integer level = 1; // 0 for zero stock & 1 for reorderlevel fill
		String queryTargetPart = "";
		FileInputStream fis;

		try {
			fis = new FileInputStream("C:/codestudio/bvasprocess/partlist/partlistNY.txt");

			Scanner scanner = new Scanner(fis);

			queryTargetPart = "select (safetyquantity - unitsinstock ) as wanted from parts where partno = ? and safetyquantity > 0";

			while (scanner.hasNextLine()) {
				String processparams = scanner.nextLine();
				String[] params = processparams.split("-");
				String targetDB = params[0].trim();
				String container = params[1].trim();
				String partlist = params[2].trim();
				BvasConFatory bvasConFactory = new BvasConFatory(targetDB);
				Connection conn = bvasConFactory.getConnection();
				PreparedStatement preparedstatement = conn.prepareStatement(queryTargetPart);

				System.out.println("processing for " + partlist);
				System.out.println("____________________________________");

				Map<String, StockContainer> stockcontainerMap = getStockContainerFromBase(baseDB, container);
				Iterator<Entry<String, StockContainer>> iter = stockcontainerMap.entrySet().iterator();

				while (iter.hasNext()) {
					Entry<String, StockContainer> mEntry = iter.next();

					// 25% value of total
					Integer wanted = 0;
					Integer checkval = (int) (0.15
							* (mEntry.getValue().getUnitsinstock() + mEntry.getValue().getContainerquantity()));

					preparedstatement.setString(1, mEntry.getKey()); // set

					// input
					// parameter
					// 1

					ResultSet rs = preparedstatement.executeQuery();

					if ((rs != null) && rs.next()) {
						wanted = rs.getInt("wanted");

						if (wanted <= 0) {
							iter.remove();
						} else {
							if (wanted > checkval) {
								wanted = checkval;

								if (wanted == 0) {
									iter.remove();
								}
							}

							mEntry.getValue().setRequired(wanted);
						}
					} else {
						iter.remove();
					}

					rs.close();
				} // while loop

				preparedstatement.close();
				conn.close();
				System.out.println("....creating partlist");
				createPartList(stockcontainerMap, baseDB, partlist);
				deleteOldListParts(partlist, baseDB);
				System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
			} // while scanner

			System.out.println("END");
			scanner.close();
		} catch (FileNotFoundException e) {
			System.out.println("Reading file line by line in Java using Scanner");
		} catch (SQLException e1) {
			System.out.println(e1.toString());
		}
	}

	private static void updateUnitsOnOrder(String partlist, String targetDB, String baseDB) {
		String sqlOrder = "select PartNo, Quantity from  vendorordereditems where orderno=?";
		String updateUnitsOnOrderSQL1 = "UPDATE  parts p SET p.UnitsOnOrder = p.UnitsOnOrder + ? WHERE  p.partno = ?";
		String updateUnitsOnOrderSQL2 = "UPDATE  parts p SET p.UnitsOnOrder = p.UnitsOnOrder + ? WHERE  p.interchnageno = ?";

		try {
			BvasConFatory bvasConFactoryBase = new BvasConFatory(baseDB);
			Connection connBase = bvasConFactoryBase.getConnection();
			PreparedStatement psBase = connBase.prepareStatement(sqlOrder);

			BvasConFatory bvasConFactoryTarget = new BvasConFatory(targetDB);
			Connection connTarget = bvasConFactoryTarget.getConnection();
			PreparedStatement statmt1Target = connTarget.prepareStatement(updateUnitsOnOrderSQL1);
			PreparedStatement statmt2Target = connTarget.prepareStatement(updateUnitsOnOrderSQL2);

			psBase.setInt(1, Integer.parseInt(partlist));
			ResultSet rsBase = psBase.executeQuery();

			while (rsBase.next()) {
				String partno = rsBase.getString("PartNo");
				Integer quantity = rsBase.getInt("Quantity");

				statmt1Target.setInt(1, quantity); // set
				statmt1Target.setString(2, partno.trim()); // set
				statmt1Target.executeUpdate();

				statmt1Target.setInt(1, quantity); // set
				statmt1Target.setString(2, partno.trim()); // set
				statmt1Target.executeUpdate();

			}

			statmt1Target.close();
			statmt2Target.close();
			rsBase.close();
			psBase.close();
			connBase.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		}

	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
