package com.bestvalue.routine;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.bestvalue.dto.SubtractQuantity;
import com.bestvalue.ultimateutils.utils.BvasConFatory;
import com.bestvalue.ultimateutils.utils.DateUtils;

public class GRCHTransfer {

	static String dbFrom = "GR";
	static String dbTo = "CH";
	static Integer orderno = 839012;
	static String regex = "^[a-zA-Z0-9]+$";
	static Pattern pattern = Pattern.compile(regex);
	static String transfercode = dbFrom + dbTo;
	static Integer transfersupplierid = 37;
	static String type = "P";

	private static List<SubtractQuantity> getSubtractQuantities(Integer orderno, String dbfrom) {
		String sql = "SELECT p.actualprice,p.interchangeno, v.* FROM vendorordereditems v , parts p  WHERE v.orderno =? AND p.partno = v.partno ORDER BY v.noorder";

		System.out.println(sql);

		Connection conn = null;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		List<SubtractQuantity> lqs = new ArrayList<SubtractQuantity>();

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(dbfrom);

			conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			pstmt1.setInt(1, orderno);
			rs1 = pstmt1.executeQuery();

			while (rs1.next()) {
				SubtractQuantity sQuantity = new SubtractQuantity();

				sQuantity.setOrderno(rs1.getInt("orderno"));
				sQuantity.setPartno(rs1.getString("PartNo"));
				sQuantity.setQuantity(rs1.getInt("Quantity"));
				sQuantity.setNoorder(rs1.getInt("noorder"));
				sQuantity.setVendorpartno(rs1.getString("vendorpartno"));
				sQuantity.setPrice(rs1.getFloat("actualprice"));
				sQuantity.setInterchangeno(rs1.getString("InterchangeNo"));
				System.out.println(rs1.getString("PartNo") + ":" + rs1.getString("Quantity"));
				lqs.add(sQuantity);
			}

			System.out.println(lqs.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} finally {
			if (pstmt1 != null) {
				try {
					pstmt1.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}

		return lqs;
	}

	private static void insertBranchTransfer(Integer orderno2, String transfercode2, String type2) {
		String sql = "insert into branchtransfer (Orderno, TransferCode, TYPE, RemoveDate) values(?,?,?,?)";
		Connection connection = null;
		PreparedStatement insertStatement = null;
		BvasConFatory bvasConFactory = new BvasConFatory(dbFrom);

		try {
			connection = bvasConFactory.getConnection();

			// connection.setAutoCommit(false);
			insertStatement = connection.prepareStatement(sql);
			insertStatement.setInt(1, orderno2);
			insertStatement.setString(2, transfercode2);
			insertStatement.setString(3, type2);
			insertStatement.setDate(4, DateUtils.getCurrentSQLDate());
			insertStatement.executeUpdate();

			// connection.commit();
		} catch (SQLException e) {
			System.out.println(e.toString());

			try {
				connection.rollback();
			} catch (SQLException e1) {
				System.out.println(e.toString());
			}
		} finally {
			if (insertStatement != null) {
				try {
					insertStatement.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}
	}

	private static void insertLocalOrders(List<SubtractQuantity> lqs, Integer orderno2, String dbFrom2,
			Integer transfersupplierid) {
		String insertSql = "insert into localorders (InvoiceNo, DateEntered, SupplierID, PartNo, VendorPartNo, Quantity, Price, VendorInvNo, VendorInvDate) values"
				+ " (?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement preparedStatementInsert = null;
		BvasConFatory bvasConFactory = new BvasConFatory(dbFrom);

		try {
			connection = bvasConFactory.getConnection();
			connection.setAutoCommit(false);

			for (SubtractQuantity subractquantity : lqs) {
				preparedStatementInsert = connection.prepareStatement(insertSql);
				preparedStatementInsert.setInt(1, subractquantity.getOrderno());
				preparedStatementInsert.setDate(2, DateUtils.getCurrentSQLDate());
				preparedStatementInsert.setInt(3, transfersupplierid);
				preparedStatementInsert.setString(4, subractquantity.getPartno());
				preparedStatementInsert.setString(5, subractquantity.getVendorpartno());
				preparedStatementInsert.setInt(6, (-1 * subractquantity.getQuantity()));
				preparedStatementInsert.setFloat(7, subractquantity.getPrice());
				preparedStatementInsert.setInt(8, subractquantity.getOrderno());
				preparedStatementInsert.setDate(9, DateUtils.getCurrentSQLDate());
				preparedStatementInsert.executeUpdate();
			}

			connection.commit();
		} catch (SQLException e) {
			System.out.println(e.toString());

			try {
				connection.rollback();
			} catch (SQLException e1) {
				System.out.println(e.toString());
			}
		} finally {
			if (preparedStatementInsert != null) {
				try {
					preparedStatementInsert.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}
	}

	private static void insertPartChanges(List<SubtractQuantity> lqs, Integer orderno, String dbFrom,
			Integer transfersupplierid) {
		String insertpartchangesSql = "INSERT INTO partschanges (PartNo, ModifiedBy, ModifiedDate, ModifiedOrder, Remarks)  values (?, ?, ?, ?, ? )";
		Connection connection = null;
		PreparedStatement insertStatement = null;
		BvasConFatory bvasConFactory = new BvasConFatory(dbFrom);

		try {
			connection = bvasConFactory.getConnection();
			connection.setAutoCommit(false);

			for (SubtractQuantity subractquantity : lqs) {
				Integer max = maXnumR(subractquantity.getPartno(), dbFrom) + 1;

				System.out.println("max = " + max);
				insertStatement = connection.prepareStatement(insertpartchangesSql);
				insertStatement.setString(1, subractquantity.getPartno());
				insertStatement.setString(2, "PRG");
				insertStatement.setDate(3, DateUtils.getCurrentSQLDate());
				insertStatement.setInt(4, max);
				insertStatement.setString(5, "ADDED IN ERROR: Quantity  transfered to supplierid: " + transfersupplierid
						+ " " + +subractquantity.getQuantity() + " Order#" + orderno);
				insertStatement.executeUpdate();
				connection.commit();
			}
		} catch (SQLException e) {
			System.out.println(e.toString());

			try {
				connection.rollback();
			} catch (SQLException e1) {
				System.out.println(e.toString());
			}
		} finally {
			if (insertStatement != null) {
				try {
					insertStatement.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}
	}

	public static void main(String[] args) {
		System.out.println("Processing.................." + orderno);

		List<SubtractQuantity> lqs = getSubtractQuantities(orderno, dbFrom);

		updateParts(lqs, orderno, dbFrom, transfersupplierid);
		insertLocalOrders(lqs, orderno, dbFrom, transfersupplierid);
		insertPartChanges(lqs, orderno, dbFrom, transfersupplierid);
		insertBranchTransfer(orderno, transfercode, type);
		System.out.println("Finished Processing..................");
	}

	private static int maXnumR(String partno, String dbfrom) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String maxSQL = "SELECT max(modifiedorder) as maxnum FROM   partschanges  WHERE partno = ?";
		int maxnumR = 0;

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(dbfrom);

			conn = bvasConFactory.getConnection();
			pstmt = conn.prepareStatement(maxSQL);
			pstmt.setString(1, partno);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				maxnumR = rs.getInt("maxnum");
			}

			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());

			return 0;
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}

		return maxnumR;
	}

	private static void updateParts(List<SubtractQuantity> lqs, Integer orderno2, String dbFrom2,
			Integer transfersupplierid) {
		String updtInvoiceSql = "UPDATE parts  SET " + " UnitsInStock = UnitsInStock - ? WHERE partno=?";
		String updtInvoiceSql2 = "UPDATE parts  SET " + " UnitsInStock = UnitsInStock - ? WHERE interchangeno=?";
		Connection connection = null;
		PreparedStatement preparedStatementUpdate = null;
		PreparedStatement preparedStatementInterchange = null;
		BvasConFatory bvasConFactory = new BvasConFatory(dbFrom);

		try {
			connection = bvasConFactory.getConnection();
			connection.setAutoCommit(false);

			for (SubtractQuantity subractquantity : lqs) {
				System.out.println("____________________________");
				System.out.println(subractquantity.getPartno());
				if (subractquantity.getInterchangeno().equalsIgnoreCase("")) {
					preparedStatementUpdate = connection.prepareStatement(updtInvoiceSql);
					preparedStatementUpdate.setInt(1, subractquantity.getQuantity());
					preparedStatementUpdate.setString(2, subractquantity.getPartno());
					int i = preparedStatementUpdate.executeUpdate();
					System.out.println("Rows Affected i" + i);
				} else {
					Matcher matcher = pattern.matcher(subractquantity.getInterchangeno());

					preparedStatementUpdate = connection.prepareStatement(updtInvoiceSql);
					preparedStatementUpdate.setInt(1, subractquantity.getQuantity());
					preparedStatementUpdate.setString(2, subractquantity.getInterchangeno());
					int j = preparedStatementUpdate.executeUpdate();
					System.out.println("Rows Affected j" + j);

					if (matcher.matches()) {
						preparedStatementInterchange = connection.prepareStatement(updtInvoiceSql2);
						preparedStatementInterchange.setInt(1, subractquantity.getQuantity());
						preparedStatementInterchange.setString(2, subractquantity.getInterchangeno());
						int k = preparedStatementInterchange.executeUpdate();
						System.out.println("Rows Affected k" + k);
					} else {
						System.out.println("CHECK INTERCHANGE FOR PARTNO:" + subractquantity.getPartno());
					}

				}

				connection.commit();
			}

		} catch (SQLException e) {
			System.out.println(e.toString());

			try {
				connection.rollback();
			} catch (SQLException e1) {
				System.out.println(e.toString());
			}
		} finally {
			if (preparedStatementInterchange != null) {
				try {
					preparedStatementInterchange.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (preparedStatementUpdate != null) {
				try {
					preparedStatementUpdate.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}
	}

}
