package com.bestvalue.routine;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.StringTokenizer;

//~--- non-JDK imports --------------------------------------------------------

import com.bestvalue.ultimateutils.utils.BvasConFatory;
import com.bestvalue.ultimateutils.utils.DateUtils;

public class CreateNewOrders {
	public static void createOrder(int supplierId, int orderNo, int whoseNo, boolean qtyIsThere, String db) {
		File fileData = new File("C:\\codestudio\\bvasprocess\\order\\order.txt");
		FileReader reader = null;

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(db);
			java.sql.Connection conn = bvasConFactory.getConnection();

			reader = new FileReader(fileData);

			@SuppressWarnings("resource")
			BufferedReader dataFile = new BufferedReader(reader);
			Statement stmt = conn.createStatement();
			ResultSet rsX = null;
			Statement stmtX = conn.createStatement();
			Statement stmtZ = conn.createStatement();
			Statement stmtY = conn.createStatement();

			stmtX.execute(
					"Insert Into VendorOrder (OrderNo, SupplierId, OrderDate, DeliveredDate, OrderStatus, OrderTotal, UpdatedInventory, UpdatedPrices) values ("
							+ orderNo + ", " + supplierId + ", '"
							+ DateUtils.convertUSToMySQLFormat(DateUtils.getNewUSDate()) + "', '"
							+ DateUtils.convertUSToMySQLFormat(DateUtils.getNewUSDate())
							+ "', 'New', 0.0, 'N', 'N' ) ");

			String line = "";
			int cnt1 = 0;

			while ((line = dataFile.readLine()) != null) {
				cnt1++;
				System.out.println(cnt1);

				String partNo = "";
				String vendPartNo = "";
				int qty = 0;
				double price = 0.0;
				int noOrder = 0;
				StringTokenizer st = new StringTokenizer(line.trim(), "\t");
				int cnt = 0;

				while (st.hasMoreTokens()) {
					cnt++;

					if ((cnt == 1) && (whoseNo == 1)) {
						partNo = st.nextToken().trim();

					} else if ((cnt == 1) && (whoseNo == 2)) {
						vendPartNo = st.nextToken().trim();
					}

					if ((cnt == 2) && qtyIsThere) {
						qty = Integer.parseInt(st.nextToken().trim());
					} else if (cnt == 2) {
						qty = 1;
					}

					if (cnt > 2) {
						break;
					}
				}

				noOrder = cnt1;
				stmtY = conn.createStatement();

				if (whoseNo == 1) {
					rsX = stmtY.executeQuery("Select * from VendorItems Where SupplierId=" + supplierId
							+ " and PartNo = '" + partNo + "'");

					if (rsX.next()) {
						vendPartNo = rsX.getString("VendorPartNo");
					}

					if (vendPartNo == null) {
						vendPartNo = "";
					}
				} else if (whoseNo == 2) {
					rsX = stmtY.executeQuery("Select * from VendorItems Where SupplierId=" + supplierId
							+ " and VendorPartNo = '" + vendPartNo + "'");

					if (rsX.next()) {
						partNo = rsX.getString("PartNo");
					} else {
						continue;
					}

					if (partNo == null) {
						partNo = "";
					}
				}
				// System.out.println(
				// "INSERT INTO VendorOrderedItems (OrderNo, PartNo,
				// VendorPartNo, Quantity, Price, NoOrder) VALUES ('"
				// + orderNo + "', '" + partNo + "', '" + vendPartNo + "', " +
				// qty + ", " + price + ", "
				// + noOrder + ")");
				stmtZ.execute(
						"INSERT INTO VendorOrderedItems (OrderNo, PartNo, VendorPartNo, Quantity, Price, NoOrder) VALUES ('"
								+ orderNo + "', '" + partNo + "', '" + vendPartNo + "', " + qty + ", " + price + ", "
								+ noOrder + ")");
			}

			// stmt.execute("");
			rsX.close();
			stmt.close();
			stmtX.close();
			stmtY.close();
			stmtZ.close();
			System.out.println("Finished Processing---");
			conn.close();
		} catch (SQLException e) {
			System.out.println("Exception---" + e);
		} catch (Exception e) {
			System.out.println("Exception---" + e);
		}
	}

	public static void main(String[] args) {
		int supplierId = 0;
		int orderNo = 0;
		int whoseNo = 0;
		boolean qtyIsThere = false;
		String db = "CH";
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Please enter the New Order No :  ");

		try {
			orderNo = Integer.parseInt(reader.readLine());
		} catch (Exception e) {
			System.out.println(e);
		}

		System.out.print("Please enter the Supplier Id :  ");

		try {
			supplierId = Integer.parseInt(reader.readLine());
		} catch (Exception e) {
			System.out.println(e);
		}

		// System.out.print("Please enter by whose no's to create the order : ");
		// System.out.print("1 -- By Our Nos\n2 -- Vendor Nos");

		try {
			whoseNo = Integer.parseInt("1");
		} catch (Exception e) {
			System.out.println(e);
		}

		// System.out.print("Does the Input file has Quantity : (Y/N) ");

		try {
			String qtyYes = "Y";

			if (qtyYes.trim().equalsIgnoreCase("Y")) {
				qtyIsThere = true;
			}
		} catch (Exception e) {
			System.out.println(e);
		}

		createOrder(supplierId, orderNo, whoseNo, qtyIsThere, db);

	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
