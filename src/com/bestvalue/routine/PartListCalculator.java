package com.bestvalue.routine;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.bestvalue.dto.ReorderParts;
import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class PartListCalculator {
	public static void main(String[] args) {
		System.out.println("******************Processing Partlist***********");

		String baseDB = "localCH";
		FileInputStream fis;
		Connection connCH = null;
		Connection connGR = null;
		Connection connNY = null;
		Connection connAM = null;

		try {
			fis = new FileInputStream("C:/codestudio/bvasprocess/partlist/partlistcalculator.txt");
			Scanner scanner = new Scanner(fis);

			BvasConFatory bvasConFactoryBase = new BvasConFatory(baseDB);
			connCH = bvasConFactoryBase.getConnection();

			while (scanner.hasNextLine()) {
				Integer totalquantity = 0;
				Integer incomingquantity = 0;

				String processparams = scanner.nextLine().trim();
				String[] params = processparams.split("-");
				String container = params[0].trim();
				String grdb = params[1].trim();
				String grorder = params[2].trim();
				String nydb = params[3].trim();
				String nyorder = params[4].trim();
				String amdb = params[5].trim();
				String amorder = params[6].trim();
				System.out.println("container : " + container);
				System.out.println("______________________________________");
				System.out.println(grdb + " " + grorder);
				System.out.println(nydb + " " + nyorder);
				System.out.println(amdb + " " + amorder);
				System.out.println("______________________________________");

				BvasConFatory bvasConFactoryGR = new BvasConFatory(grdb);
				connGR = bvasConFactoryGR.getConnection();

				BvasConFatory bvasConFactoryNY = new BvasConFatory(nydb);
				connNY = bvasConFactoryNY.getConnection();

				BvasConFatory bvasConFactoryAM = new BvasConFatory(amdb);
				connAM = bvasConFactoryAM.getConnection();

				// get the partlist coming
				List<ReorderParts> partscoming = getParts(Integer.parseInt(container), baseDB);
				for (ReorderParts reorderparts : partscoming) {
					System.out.println(reorderparts.getCount());
				}

			} // SCANNER
			System.out.println("END");
			scanner.close();

		} catch (FileNotFoundException e) {

			System.out.println("Error : " + e.toString());
		} catch (SQLException e) {
			System.out.println("Error : " + e.toString());
		} finally {
			/*
			 * if (pstmt1 != null) { try { pstmt1.close(); } catch (SQLException e) {
			 * System.out.println(e.toString()); } }
			 */

			if (connCH != null) {
				try {
					connCH.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
			if (connGR != null) {
				try {
					connGR.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
			if (connNY != null) {
				try {
					connNY.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
			if (connAM != null) {
				try {
					connAM.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}

	} // main

	private static List<ReorderParts> getParts(Integer orderno, String db) {
		String sql = "SELECT partno,quantity from vendorordereditems where orderno =" + orderno;

		// System.out.println(sql);

		Connection conn = null;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		List<ReorderParts> lqs = new ArrayList<ReorderParts>();

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(db);

			conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery();

			while (rs1.next()) {
				ReorderParts rp = new ReorderParts();

				rp.setPartno(rs1.getString("partno"));
				rp.setCount(rs1.getInt("quantity"));
				lqs.add(rp);
			}

			// System.out.println(lqs.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} finally {
			if (pstmt1 != null) {
				try {
					pstmt1.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}

		return lqs;
	}
}
