package com.bestvalue.routine;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.collections4.MultiMap;
import org.apache.commons.collections4.map.MultiValueMap;

import com.bestvalue.dto.SelectedOrderItems;
import com.bestvalue.ultimateutils.utils.BvasConFatory;
import com.bestvalue.ultimateutils.utils.DateUtils;
import com.bestvalue.ultimateutils.utils.NumberUtils;

public class EnhancedCompare {
	private static List<String> getPartsFromMainOrder(String orders, String targetDB) {
		List<String> partsList = new ArrayList<String>();
		String sql = "SELECT distinct partno FROM vendorordereditems  WHERE orderno in (" + orders + " )"
				+ " ORDER BY partno";

		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		try {
			BvasConFatory bvasConFactory = new BvasConFatory(targetDB);
			java.sql.Connection conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery(sql);
			while (rs1.next()) {
				partsList.add(rs1.getString("PartNo"));
			}
			System.out.println(partsList.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

		return partsList;
	}

	private static List<SelectedOrderItems> getSelectedOrderItems(List<String> lqs, String targetDB, String orders) {
		List<SelectedOrderItems> lso = new ArrayList<SelectedOrderItems>();
		MultiMap<Integer, String> multiMap = new MultiValueMap<Integer, String>();
		try {
			String[] ord = orders.split(",");
			BvasConFatory bvasConFactory = new BvasConFatory(targetDB);
			PreparedStatement pstmt1 = null;
			PreparedStatement pstmt2 = null;
			ResultSet rs1 = null;
			java.sql.Connection conn = bvasConFactory.getConnection();
			System.out.println("___________________________________________________________________________");
			for (String partno : lqs) {
				String str1 = "SELECT price , orderno, quantity FROM vendorordereditems WHERE partno = '" + partno
						+ "' " + "AND orderno in (" + orders + ") "
						// + " AND orderno in (3000015,3000016,3000017) "
						+ " AND price > 0 " + "ORDER BY price,orderno desc  LIMIT 1";
				pstmt1 = conn.prepareStatement(str1);
				rs1 = pstmt1.executeQuery(str1);
				while (rs1.next()) {
					SelectedOrderItems sOrderItems = new SelectedOrderItems();
					sOrderItems.setPartno(partno);
					sOrderItems.setSupplierid(rs1.getInt("orderno"));
					sOrderItems.setOrderno(rs1.getInt("orderno"));
					sOrderItems.setPrice(rs1.getFloat("price"));
					sOrderItems.setQuantity(rs1.getInt("quantity"));
					lso.add(sOrderItems);
					multiMap.put(rs1.getInt("orderno"), "'" + partno + "'");

					// get all the set of keys

					System.out.println(partno + ", " + rs1.getString("orderno") + ", " + rs1.getString("price") + ", "
							+ rs1.getString("quantity"));
				}

			}
			for (String key : ord) {
				System.out.println("Key = " + key);
				String deleteSql = "";
				if (null != multiMap.get(Integer.parseInt(key))) {
					String vals = (multiMap.get(Integer.parseInt(key))).toString();
					System.out.println("Values = " + vals.substring(1, vals.length() - 1) + "\n");
					if (vals.substring(1, vals.length() - 1).equalsIgnoreCase("")) {
						deleteSql = "DELETE FROM VENDORORDEREDITEMS WHERE ORDERNO = " + Integer.parseInt(key);
					} else {
						deleteSql = "DELETE FROM VENDORORDEREDITEMS WHERE ORDERNO = " + Integer.parseInt(key)
								+ " AND PARTNO NOT IN (" + vals.substring(1, vals.length() - 1) + ")";
					}
					System.out.println(vals.substring(1, vals.length() - 1));
					pstmt2 = conn.prepareStatement(deleteSql);
					pstmt2.executeUpdate();
				}
			}

			if (rs1 != null) {
				rs1.close();
			}
			if (pstmt1 != null) {
				pstmt1.close();
			}
			if (pstmt2 != null) {
				pstmt2.close();
			}
			if (conn != null) {
				conn.close();
			}

			String listof5 = "";
			String listof6 = "";
			String listof11 = "";
			String listof14 = "";
			String listof3 = "";
			String listof27 = "";
			String listof2 = "";

			for (SelectedOrderItems sOrderItems : lso) {
				switch (sOrderItems.getSupplierid()) {
				case 5:
					listof5 = listof5 + ", " + sOrderItems.getPartno();
					break;
				case 6:
					listof6 = listof6 + ", " + sOrderItems.getPartno();
					break;
				case 11:
					listof11 = listof11 + ", " + sOrderItems.getPartno();
					break;
				case 14:
					listof14 = listof14 + ", " + sOrderItems.getPartno();
					break;
				case 3:
					listof3 = listof3 + ", " + sOrderItems.getPartno();
					break;
				case 27:
					listof27 = listof27 + ", " + sOrderItems.getPartno();
					break;
				case 2:
					listof2 = listof2 + ", " + sOrderItems.getPartno();
					break;
				}
			}
			// System.out.println(listof14);
			System.out.println("end processing");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lso;
	}

	public static void main(String[] args) {
		System.out.println("-----start of compare process-----");
		String targetDB = "CH";
		String orders = "58361,58362,58363,58364,58365,58366,58367";
		// String orders = "54628,54629";

		List<String> lqs = getPartsFromMainOrder(orders, targetDB);
		List<SelectedOrderItems> lso = getSelectedOrderItems(lqs, targetDB, orders);

		String[] on = orders.split(",");
		for (String orderNo : on) {
			makeFile(orderNo.trim(), targetDB);
		}
		System.out.println("-----end of compare process-----");
	}

	public static void makeFile(String orderNo, String targetDB) {

		try {
			File returns = new File("c:/codestudio/bvasprocess/confirmation/OrderCompared" + orderNo + ".txt");
			FileWriter wrt = new FileWriter(returns);
			BvasConFatory bvasConFactory = new BvasConFatory(targetDB);

			Statement stmt = null;
			PreparedStatement pstmt1 = null;
			Statement pstmt2 = null;
			Statement stmt3 = null;
			ResultSet rs3 = null;
			Statement stmt4 = null;
			ResultSet rs4 = null;
			ResultSet rs0 = null;
			ResultSet rs1 = null;
			Statement stmtXX = null;
			ResultSet rsXX = null;
			ResultSet rs2 = null;
			Statement stmtA = null;
			ResultSet rsA = null;
			Statement stmtXXX = null;
			Statement stmt5 = null;
			Statement stmt6 = null;
			ResultSet rs6 = null;
			Statement stmt52 = null;
			ResultSet rs52 = null;
			Statement stmt51 = null;
			ResultSet rs51 = null;
			ResultSet rs5 = null;
			ResultSet rsXXX = null;

			Connection conn = bvasConFactory.getConnection();
			stmt = conn.createStatement();
			pstmt1 = conn.prepareStatement(
					"Select a.PartNo, a.VendorPartNo, a.Price, a.Quantity, b.CostPrice, b.ActualPrice, b.UnitsInStock, b.ReorderLevel, b.InterchangeNo, b.Year, b.PartDescription, c.MakeModelName from VendorOrderedItems a, Parts b, MakeModel c where a.OrderNo="
							+ orderNo
							+ " and a.partno=b.partno and b.MakeModelCode=c.MakeModelCode Order By a.PartNo ");
			pstmt2 = conn.createStatement();
			stmt3 = conn.createStatement();
			rs3 = stmt3.executeQuery(
					"Select SupplierId, SUBSTRING(CompanyName, 1,3) from Vendors where CompanyName <> '' ");
			Hashtable<String, String> vendorTable = new Hashtable<String, String>();

			while (rs3.next()) {
				// System.out.println(rs3.getString(1));
				vendorTable.put(rs3.getString(1), rs3.getString(2));
			}
			stmt4 = conn.createStatement();
			rs4 = stmt4.executeQuery("Select SupplierId, CompanyName from LocalVendors ");
			Hashtable<String, String> localVendorTable = new Hashtable<String, String>();

			while (rs4.next()) {
				localVendorTable.put(rs4.getString(1), rs4.getString(2));
			}
			int supId = 0;
			rs0 = stmt.executeQuery("SELECT SupplierId FROM VendorOrder WHERE OrderNo=" + orderNo);
			if (rs0.next()) {
				supId = rs0.getInt("SupplierId");
			} else {
				System.out.println("No SupplierId");
				System.exit(1);
			}
			pstmt1.clearParameters();
			rs1 = pstmt1.executeQuery();
			wrt.write("PRICE COMPARISON FOR THE ORDER : " + orderNo);
			wrt.write("\n");
			wrt.write("\n");
			while (rs1.next()) {
				System.out.println(rs1.getString("PartNo"));
				String partNo = rs1.getString("PartNo");

				String vendPartNo = rs1.getString("VendorPartNo");
				String vendDesc = "";
				double price = rs1.getDouble("Price");
				int qty = rs1.getInt("Quantity");
				double costPrice = rs1.getDouble("CostPrice");
				double actualPrice = rs1.getDouble("ActualPrice");
				int units = rs1.getInt("UnitsInStock");
				int reorder = rs1.getInt("ReorderLevel");
				String descrip = rs1.getString("Year") + "  " + rs1.getString("PartDescription") + "  "
						+ rs1.getString("MakeModelName");
				String remarkStr = "";
				String interNo = rs1.getString("InterchangeNo");
				if (!interNo.trim().equals("")) {
					stmtXX = conn.createStatement();
					rsXX = stmtXX.executeQuery("Select UnitsInStock from parts where partno='" + interNo + "'");
					if (rsXX.next()) {
						units = rsXX.getInt(1);
					}
				}
				String p1 = "";
				String p2 = "";
				stmtA = conn.createStatement();
				String sqlA = "Select PartNo From Parts Where InterchangeNo='";
				if (interNo.trim().equals("")) {
					sqlA += partNo + "'";
				} else {
					sqlA += interNo + "' and partNo!='" + partNo + "'";
				}
				rsA = stmtA.executeQuery(sqlA);
				while (rsA.next()) {
					String xxx = rsA.getString(1);
					if (p1.trim().equals("")) {
						p1 = xxx;
					} else if (p2.trim().equals("")) {
						p2 = xxx;
					} else {
						break;
					}
				}
				if (vendPartNo != null && !vendPartNo.trim().equals("")) {
					String it1 = "";
					String it2 = "";
					stmtXXX = conn.createStatement();
					rsXXX = stmtXXX.executeQuery(
							"Select ItemDesc1, ItemDesc2 From VendorItems Where VendorPartNo = '" + vendPartNo + "' ");
					if (partNo.equalsIgnoreCase("T7107")) {
						System.out.println("Select ItemDesc1, ItemDesc2 From VendorItems Where VendorPartNo like '"
								+ vendPartNo + "%' ");
					}
					if (rsXXX.next()) {
						vendDesc = "";
						it1 = rsXXX.getString("ItemDesc1");
						it2 = rsXXX.getString("ItemDesc2");
						if (it1 != null && !it1.trim().equals("")) {
							vendDesc = it1.trim();
						}
						if (it2 != null && !it2.trim().equals("")) {
							vendDesc += it2.trim();
						}
					}
				}
				String printStr1X = "";
				if (costPrice != 0 && price != 0) {
					if ((((costPrice - price) / costPrice) * 100) > 40) {
						printStr1X += "COST :  GOOD";
					} else {
						printStr1X += "COST : CHECK";
					}
					if (printStr1X.trim().equals("")) {
						printStr1X += "            ";
					}
				}

				double lowestPrice = price;
				double price1 = 0;
				double price2 = 0;
				double price3 = 0;
				double price4 = 0;
				double price5 = 0;
				int supId1 = 0;
				int supId2 = 0;
				int supId3 = 0;
				int supId4 = 0;
				int supId5 = 0;
				String supName1 = "";
				String supName2 = "";
				String supName3 = "";
				String supName4 = "";
				String supName5 = "";

				String sqle = "Select SupplierId, SellingRate from VendorItems Where (PartNo like '%" + partNo.trim()
						+ "%'";
				if (!interNo.trim().equals("")) {
					sqle += " or PartNo like '%" + interNo + "%'";
				}
				if (!p1.trim().equals("")) {
					sqle += " or PartNo like '%" + p1 + "%'";
				}
				if (!p2.trim().equals("")) {
					sqle += " or PartNo like '%" + p2 + "%'";
				}
				sqle += ") and SupplierId != " + supId + " and SellingRate != 0 Order By SellingRate";
				rs2 = pstmt2.executeQuery(sqle);

				if (rs2.next()) {
					supId1 = rs2.getInt("SupplierId");
					price1 = rs2.getDouble("SellingRate");
				}
				if (rs2.next()) {
					supId2 = rs2.getInt("SupplierId");
					price2 = rs2.getDouble("SellingRate");
				}
				if (rs2.next()) {
					supId3 = rs2.getInt("SupplierId");
					price3 = rs2.getDouble("SellingRate");
				}
				if (rs2.next()) {
					supId4 = rs2.getInt("SupplierId");
					price4 = rs2.getDouble("SellingRate");
				}
				if (rs2.next()) {
					supId5 = rs2.getInt("SupplierId");
					price5 = rs2.getDouble("SellingRate");
				}

				String remarks = "";

				if (price5 != 0) {
					if (price >= price5) {
						remarks = "Hi";
					}
				} else if (price4 != 0) {
					if (price >= price4) {
						remarks = "Hi";
					}
				} else if (price3 != 0) {
					if (price >= price3) {
						remarks = "Hi";
					}
				} else if (price2 != 0) {
					if (price >= price2) {
						remarks = "Hi";
					}
				} else if (price1 != 0) {
					if (price >= price1) {
						remarks = "Hi";
					}
				}
				if (price1 != 0) {
					if (price <= price1) {
						remarks = "Lo";
					}

					// For Checking The Lowest Price
					if (price1 < lowestPrice) {
						lowestPrice = price1;
					}
				}
				if (remarks.trim().equals("")) {
					remarks = "Mid";
				}

				if (supId1 != 0) {
					supName1 = vendorTable.get(supId1 + "");
				}
				if (supId2 != 0) {
					supName2 = vendorTable.get(supId2 + "");
				}
				if (supId3 != 0) {
					supName3 = vendorTable.get(supId3 + "");
				}
				if (supId4 != 0) {
					supName4 = vendorTable.get(supId4 + "");
				}
				if (supId5 != 0) {
					supName5 = vendorTable.get(supId5 + "");
				}

				if (supId1 == 1 || supId2 == 1 || supId3 == 1 || supId4 == 1 || supId5 == 1) {
					remarkStr += "-EE-";
				}
				if (supId1 == 13 || supId2 == 13 || supId3 == 13 || supId4 == 13 || supId5 == 13) {
					remarkStr += "-AC-";
				}

				String printStr1 = "";
				String printStr2 = "";
				String printStr3 = "";
				String printStr31 = "";
				// String printStr32 = "";
				String printStr32X = "";
				String printStr32Y = "";
				String printStr32Z = "";
				String printStr4 = "";
				String printStrXX1 = "BV :: " + descrip;
				String printStrXX2 = "VD :: " + vendDesc.trim();

				printStr1 = partNo + ":" + price + " (" + remarks + ")" + " QTY:" + qty + " CST:" + costPrice + " ACT:"
						+ actualPrice + " ST:" + units + " RE:" + reorder;
				// printStr1 = partNo + " : " + price + " (" + remarks + ")";
				if (price1 != 0) {
					printStr2 = "Prices:   " + price1 + " (" + supName1 + ") ";
				}
				if (price2 != 0) {

					printStr2 += "\t" + price2 + " (" + supName2 + ") ";
				}
				if (price3 != 0) {
					printStr2 += "\t" + price3 + " (" + supName3 + ") ";
				}
				if (price4 != 0) {
					printStr2 += "\t" + price4 + " (" + supName4 + ") ";
				}
				if (price5 != 0) {
					printStr2 += "\t" + price5 + " (" + supName5 + ") ";
				}

				String sqlx = "Select b.SupplierId, b.OrderDate, a.OrderNo, a.Price, a.Quantity from VendorOrderedItems a, VendorOrder b Where (a.PartNo like '%"
						+ partNo.trim() + "%'";
				if (!interNo.trim().equals("")) {
					sqlx += " or a.PartNo like '%" + interNo + "%'";
				}
				if (!p1.trim().equals("")) {
					sqlx += " or a.PartNo like '%" + p1 + "%'";
				}
				if (!p2.trim().equals("")) {
					sqlx += " or a.PartNo like '%" + p2 + "%'";
				}
				sqlx += ") and a.OrderNo != " + orderNo
						+ " and a.Price != 0 and a.OrderNo=b.OrderNo and b.IsFinal='Y' and b.UpdatedInventory='N' Order By a.OrderNo ";
				stmt5 = conn.createStatement();
				rs5 = stmt5.executeQuery(sqlx);
				printStr3 = "Committed :";
				while (rs5.next()) {
					String xx = DateUtils.convertMySQLToUSFormat(rs5.getString("OrderDate"));
					String mm = "";
					String yy = "";
					try {
						mm = xx.substring(0, 2);
						yy = xx.substring(8);
					} catch (Exception e) {
						System.out.println(e);
					}
					double prce = rs5.getDouble("Price");
					if (prce < lowestPrice) {
						lowestPrice = prce;
					}
					String ordNo = rs5.getString("OrderNo");
					int suppId = rs5.getInt("SupplierId");
					if (suppId == 1 && remarkStr.indexOf("-EE-") == -1) {
						remarkStr += "-EE-";
					}
					if (suppId == 13 && remarkStr.indexOf("-AC-") == -1) {
						remarkStr += "-AC-";
					}
					String supNm = vendorTable.get(suppId + "").substring(0, 3);
					int qt = rs5.getInt("Quantity");
					printStr3 += prce + " (" + qt + "-" + supNm + "(" + ordNo + ")" + "-" + mm + "/" + yy + ") ";
				}

				// FOR OLD ORDERS
				String sqlx1 = "Select b.SupplierId, b.OrderDate, a.OrderNo, a.Price from VendorOrderedItems a, VendorOrder b Where a.OrderNo > 3000 and (a.PartNo like '%"
						+ partNo.trim() + "%'";
				if (!interNo.trim().equals("")) {
					sqlx1 += " or a.PartNo like '%" + interNo + "%'";
				}
				if (!p1.trim().equals("")) {
					sqlx1 += " or a.PartNo like '%" + p1 + "%'";
				}
				if (!p2.trim().equals("")) {
					sqlx1 += " or a.PartNo like '%" + p2 + "%'";
				}
				sqlx1 += ") and a.OrderNo != " + orderNo
						+ " and a.Price != 0 and a.OrderNo=b.OrderNo and b.IsFinal='Y' and b.UpdatedInventory='Y' Order By a.OrderNo ";
				stmt51 = conn.createStatement();
				rs51 = stmt51.executeQuery(sqlx1);
				printStr31 = "Old :";
				while (rs51.next()) {
					String xx1 = DateUtils.convertMySQLToUSFormat(rs51.getString("OrderDate"));
					String mm1 = "";
					String yy1 = "";
					try {
						mm1 = xx1.substring(0, 2);
						yy1 = xx1.substring(8);
					} catch (Exception e) {
						System.out.println(e);
					}
					double prce = rs51.getDouble("Price");
					if (prce < lowestPrice) {
						lowestPrice = prce;
					}
					int suppId = rs51.getInt("SupplierId");
					if (suppId == 1 && remarkStr.indexOf("-EE-") == -1) {
						remarkStr += "-EE-";
					}
					if (suppId == 13 && remarkStr.indexOf("-AC-") == -1) {
						remarkStr += "-AC-";
					}
					printStr31 += prce + " (" + rs51.getString("OrderNo") + "- "
							+ vendorTable.get(suppId + "").substring(0, 3) + "-" + mm1 + "/" + yy1 + ") ";
				}

				String sqlx2 = "Select b.SupplierId, b.OrderDate, a.OrderNo, a.Price from VendorOrderedItems a, VendorOrder b Where (a.PartNo like '%"
						+ partNo.trim() + "%'";
				if (!interNo.trim().equals("")) {
					sqlx2 += " or a.PartNo like '%" + interNo + "%'";
				}
				if (!p1.trim().equals("")) {
					sqlx2 += " or a.PartNo like '%" + p1 + "%'";
				}
				if (!p2.trim().equals("")) {
					sqlx2 += " or a.PartNo like '%" + p2 + "%'";
				}
				sqlx2 += ") and a.OrderNo != " + orderNo
						+ " and a.Price != 0 and a.OrderNo=b.OrderNo and b.IsFinal='N' Order By a.OrderNo ";
				stmt52 = conn.createStatement();
				rs52 = stmt52.executeQuery(sqlx2);
				// printStr32 = "New Orders :";
				printStr32X = "Back :";
				printStr32Y = "Remo :";
				printStr32Z = "Orde :";
				while (rs52.next()) {
					String xx2 = DateUtils.convertMySQLToUSFormat(rs52.getString("OrderDate"));
					String mm2 = "";
					String yy2 = "";
					try {
						mm2 = xx2.substring(0, 2);
						yy2 = xx2.substring(8);
					} catch (Exception e) {
						System.out.println(e);
					}
					double prce = rs52.getDouble("Price");
					if (prce < lowestPrice) {
						lowestPrice = prce;
					}
					int suppId = rs52.getInt("SupplierId");
					if (suppId == 1 && remarkStr.indexOf("-EE-") == -1) {
						remarkStr += "-EE-";
					}
					if (suppId == 13 && remarkStr.indexOf("-AC-") == -1) {
						remarkStr += "-AC-";
					}
					int ordNo = Integer.parseInt(rs52.getString("OrderNo"));

					// FOR REMOVING GR ORDERS & OTHERS
					// if ((ordNo >= 526 && ordNo <= 600) || (ordNo >= 651 &&
					// ordNo <= 750)) {
					// continue;
					// FOR BACK ORDERS
					// } else if (ordNo >= 601 && ordNo <= 650) {
					if ((ordNo >= 4001 && ordNo <= 4400) || (ordNo >= 4401 && ordNo <= 4800)) {
						printStr32X += prce + " (" + ordNo + "-" + vendorTable.get(suppId + "").substring(0, 3) + "-"
								+ mm2 + "/" + yy2 + ") ";
						// REMOVED FOR HIGH PRICES
					} else if ((ordNo >= 3361 && ordNo <= 3800) || (ordNo >= 8251 && ordNo <= 9000)) {
						printStr32Y += prce + " (" + ordNo + "-" + vendorTable.get(suppId + "").substring(0, 3) + "-"
								+ mm2 + "/" + yy2 + ") ";
					} else {
						printStr32Z += prce + " (" + ordNo + "-" + vendorTable.get(suppId + "").substring(0, 3) + "-"
								+ mm2 + "/" + yy2 + ") ";
					}
				}

				String sqly = "Select SupplierId, DateEntered, Price from LocalOrders Where DateEntered>'2009-07-01' and supplierid!=38 and supplierid!=39 and (PartNo like '%"
						+ partNo.trim() + "%'";
				if (!interNo.trim().equals("")) {
					sqly += " or PartNo like '%" + interNo + "%'";
				}
				if (!p1.trim().equals("")) {
					sqly += " or PartNo like '%" + p1 + "%'";
				}
				if (!p2.trim().equals("")) {
					sqly += " or PartNo like '%" + p2 + "%'";
				}
				sqly += ") and Price != 0 ";
				stmt6 = conn.createStatement();
				rs6 = stmt6.executeQuery(sqly);
				printStr4 = "Local :";
				while (rs6.next()) {
					String xx = DateUtils.convertMySQLToUSFormat(rs6.getString("DateEntered"));
					String mm = "";
					String yy = "";
					try {
						mm = xx.substring(0, 2);
						yy = xx.substring(8);
					} catch (Exception e) {
						System.out.println(e);
					}
					int supIdd = 0;
					supIdd = rs6.getInt("SupplierId");
					if (supIdd == 4) {
						remarkStr += "-MX-";
					}
					if (supIdd == 14) {
						remarkStr += "-AC-";
					}
					String supStr = "";
					if (supIdd != 0) {
						supStr = localVendorTable.get(supIdd + "").substring(0, 3);
					}
					double prce = rs6.getDouble("Price");
					if (prce < lowestPrice) {
						lowestPrice = prce;
					}
					printStr4 += prce + " (" + supStr + "-" + mm + "/" + yy + ") ";
				}

				if (lowestPrice == price) {
					printStr1X += "      PRICE : LOWEST";
				} else if ((price - lowestPrice) < 0.50) {
					printStr1X += "      PRICE : OK";
				} else {
					printStr1X += "      PRICE : " + lowestPrice;
				}

				if (costPrice != 0) {
					double perc = ((costPrice - price) / costPrice) * 100;
					if (perc > 0) {
						perc = NumberUtils.cutFractions(perc);
					}
					if (price < 2 && perc < 70) {
						printStr1X += "          LOW COST : " + perc;
					} else if (price >= 2 && price < 5 && perc < 60) {
						printStr1X += "          LOW COST : " + perc;
					} else if (price >= 5 && price < 8 && perc < 55) {
						printStr1X += "          LOW COST : " + perc;
					} else if (price >= 8 && price < 12 && perc < 50) {
						printStr1X += "          LOW COST : " + perc;
					} else if (price >= 12 && price < 22 && perc < 45) {
						printStr1X += "          LOW COST : " + perc;
					} else if (price >= 22 && price < 42 && perc < 40) {
						printStr1X += "          LOW COST : " + perc;
					} else if (price >= 42 && price < 72 && perc < 35) {
						printStr1X += "          LOW COST : " + perc;
					} else if (price >= 72 && perc < 30) {
						printStr1X += "          LOW COST : " + perc;
					}
				}

				if (!remarkStr.trim().equals("")) {
					printStr1X += remarkStr;
				}

				if (!printStr1.trim().equals("")) {
					wrt.write("________________________________________________________");
					wrt.write("\n");
					wrt.write(printStr1);
					wrt.write("\n");
					wrt.write(printStr1X);
					wrt.write("\n");
					if (!printStr2.trim().equals("")) {
						wrt.write(printStr2);
						wrt.write("\n");
					}
					if (printStr3.trim().length() > 11) {
						wrt.write(printStr3);
						wrt.write("\n");
					}
					if (printStr31.trim().length() > 5) {
						wrt.write(printStr31);
						wrt.write("\n");
					}
					if (printStr32X.trim().length() > 6) {
						wrt.write(printStr32X);
						wrt.write("\n");
					}
					if (printStr32Y.trim().length() > 6) {
						wrt.write(printStr32Y);
						wrt.write("\n");
					}
					if (printStr32Z.trim().length() > 6) {
						wrt.write(printStr32Z);
						wrt.write("\n");
					}
					if (printStr4.trim().length() > 7) {
						wrt.write(printStr4);
						wrt.write("\n");
					}

					wrt.write(printStrXX1);
					wrt.write("\n");

					if (printStrXX2.trim().length() > 6) {
						wrt.write(printStrXX2);
						wrt.write("\n");
					}
					wrt.write("\n");

					printStr1 = "";
					printStr1X = "";
					printStr2 = "";
					printStr3 = "";
					printStr31 = "";
					printStr32X = "";
					printStr32Y = "";
					printStr32Z = "";
					printStr4 = "";
					printStrXX1 = "";
					printStrXX2 = "";

					// wrt1.write(printStr1);
					// wrt1.write("\n");
					// wrt1.write(printStr1X);
					// wrt1.write("\n\n");
				}

			} // rs1

			wrt.write("_____________END OF COMPARE_________________");

			if (rs4 != null) {
				rs4.close();
			}
			if (rs0 != null) {
				rs0.close();
			}

			if (rs2 != null) {
				rs2.close();
			}
			if (stmtA != null) {
				stmtA.close();
			}
			if (rs4 != null) {
				rs4.close();
			}
			if (rsA != null) {
				rsA.close();
			}
			if (stmt5 != null) {
				stmt5.close();
			}
			if (stmt6 != null) {
				stmt6.close();
			}
			if (rs6 != null) {
				rs6.close();
			}
			if (stmt52 != null) {
				stmt52.close();
			}
			if (stmtXXX != null) {
				stmtXXX.close();
			}

			if (rs52 != null) {
				rs52.close();
			}
			if (stmt51 != null) {
				stmt51.close();
			}
			if (rs51 != null) {
				rs51.close();
			}
			if (rs5 != null) {
				rs5.close();
			}
			if (rsXXX != null) {
				rsXXX.close();
			}

			if (stmt != null) {
				stmt.close();
			}
			if (pstmt1 != null) {
				pstmt1.close();
			}
			if (pstmt2 != null) {
				pstmt2.close();
			}
			if (stmt3 != null) {
				stmt3.close();
			}
			if (rs3 != null) {
				rs3.close();
			}
			if (rs1 != null) {
				rs1.close();
			}
			if (stmt4 != null) {
				stmt4.close();
			}

			conn.close();
			wrt.close();

			System.out.println("_____________END OF COMPARE_________________");

		} catch (IOException e) {
			System.out.println("IOException:" + e.toString());
		} catch (SQLException e) {
			System.out.println("SQLException:" + e.toString());
		}

	}
}
