package com.bestvalue.routine;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bestvalue.dto.ReorderParts;
import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class DailyOrderCheck {

	public static void main(String[] args) {
		int i = 0;
		String checkSQl = "SELECT unitsinstock,reorderlevel FROM parts WHERE partno =?";

		Integer orderno = 111223;
		Double threshold = 0.40;
		Integer noofitems = 500;

		String baseDB = "CH";
		List<ReorderParts> reparts = getParts(baseDB, orderno);
		System.out.println("start");
		try {
			for (ReorderParts rp : reparts) {
				Integer quantityinstock = 0;
				Integer reorderlevel = 0;
				int total = 0;
				String branchesstring = "CH,MP,GR";
				String[] branches = branchesstring.split(",");

				for (String branch : branches) {
					BvasConFatory bvasConFactory = new BvasConFatory(branch);
					PreparedStatement pstmt1 = null;
					ResultSet rs1 = null;
					java.sql.Connection conn = bvasConFactory.getConnection();

					pstmt1 = conn.prepareStatement(checkSQl);
					pstmt1.setString(1, rp.getPartno());
					rs1 = pstmt1.executeQuery();

					if (rs1.next()) {
						if (branch.equalsIgnoreCase("CH")) {
							reorderlevel = rs1.getInt("reorderlevel");
						}

						quantityinstock = quantityinstock + rs1.getInt("unitsinstock");
					}

					rs1.close();
					pstmt1.close();
					conn.close();
				}

				total = (int) Math.ceil(threshold * reorderlevel);

				if (quantityinstock > reorderlevel) {
					/*
					 * System.out.println("___________________________");
					 * System.out.println("quantityinstock > reorderlevel");
					 * System.out.println(i++); System.out.println("'" + rp.getPartno() + "',");
					 * System.out.println("reorderlevel:" + reorderlevel);
					 * System.out.println("total:" + total); System.out.println("quantityinstock:" +
					 * quantityinstock);
					 */
					deleteOldListParts(rp.getPartno(), baseDB, orderno);
				} else {
					if (quantityinstock > total) {
						/*
						 * System.out.println("___________________________");
						 * System.out.println("quantityinstock > total"); System.out.println(i++);
						 * System.out.println("'" + rp.getPartno() + "',");
						 * System.out.println("reorderlevel:" + reorderlevel);
						 * System.out.println("total:" + total); System.out.println("quantityinstock:" +
						 * quantityinstock);
						 */
						deleteOldListParts(rp.getPartno(), baseDB, orderno);
					}
				}

			} // for loop
			doPackage(noofitems, orderno, baseDB);

		} catch (SQLException e) {
			System.out.println("___________________________" + e.getErrorCode() + ":" + e.toString());
			e.printStackTrace();
		}

		System.out.println("all done");
	}

	private static void doPackage(Integer noofitems, Integer orderno, String baseDB) {

		BvasConFatory bvasConFactoryBase = new BvasConFatory(baseDB);

		if (orderno > 0) {
			try {
				Connection connBase = bvasConFactoryBase.getConnection();

				String sql1 = "TRUNCATE TABLE temporder;";
				PreparedStatement stmt1 = connBase.prepareStatement(sql1);
				stmt1.executeUpdate();
				stmt1.close();

				String sql2 = "INSERT INTO temporder ( SELECT partno FROM vendorordereditems WHERE orderno = ? ORDER BY  RAND() LIMIT ?);";
				PreparedStatement stmt2 = connBase.prepareStatement(sql2);
				stmt2.setInt(1, orderno); // set
				stmt2.setInt(2, noofitems); // set
				stmt2.executeUpdate();
				stmt2.close();

				String sql3 = "DELETE FROM vendorordereditems WHERE orderno = ? AND partno NOT IN (SELECT partno FROM temporder);";
				PreparedStatement stmt3 = connBase.prepareStatement(sql3);
				stmt3.setInt(1, orderno); // set
				stmt3.executeUpdate();
				stmt3.close();

				String sql4 = "UPDATE vendorordereditems vo INNER JOIN PARTS P  ON P.PartNo = VO.PartNo AND vo.Quantity <   CEIL (REORDERLEVEL * 0.30) AND vo.ORDERNO = ? SET vo.Quantity =    CEIL (REORDERLEVEL * 0.30) WHERE  P.PartNo = VO.PartNo AND vo.Quantity <    CEIL (REORDERLEVEL * 0.30) AND vo.ORDERNO = ?;";
				PreparedStatement stmt4 = connBase.prepareStatement(sql4);
				stmt4.setInt(1, orderno); // set
				stmt4.setInt(2, orderno); // set
				stmt4.executeUpdate();
				stmt4.close();

				String sql5 = "UPDATE vendorordereditems vo INNER JOIN vendoritems vi  ON vo.VendorPartNo = vi.VendorPartNo SET vo.Quantity = (CEIL(vo.Quantity / vi.NoOfPieces) ) * vi.NoOfPieces WHERE vo.Quantity > 0 AND vi.NoOfPieces >= 1 AND vo.ORDERNO = ?;";
				PreparedStatement stmt5 = connBase.prepareStatement(sql5);
				stmt5.setInt(1, orderno); // set
				stmt5.executeUpdate();
				stmt5.close();

				connBase.close();
			} catch (SQLException e) {
				System.out.println(e.toString());
			}
		}

	}

	private static void deleteOldListParts(String partno, String baseDB, Integer orderno) {

		String deletepartssql = "delete from vendorordereditems WHERE  orderno = ? and partno = ?";
		BvasConFatory bvasConFactoryBase = new BvasConFatory(baseDB);

		if (partno != null) {
			if (orderno > 0) {
				try {
					Connection connBase = bvasConFactoryBase.getConnection();

					PreparedStatement statmt1Target = connBase.prepareStatement(deletepartssql);

					statmt1Target.setInt(1, orderno); // set
					statmt1Target.setString(2, partno.trim()); // set
					statmt1Target.executeUpdate();

					statmt1Target.close();
					connBase.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}
	}

	private static List<ReorderParts> getParts(String db, Integer orderno) {
		String sql = "SELECT v.partno, v.quantity from VENDORORDEREDITEMS v , parts p  WHERE v.partno = p.partno AND v.ORDERNO =? and v.quantity >= 1 ORDER BY p.unitsonorder,  p.UnitsInStock";

		// System.out.println(sql);
		Connection conn = null;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		List<ReorderParts> lqs = new ArrayList<ReorderParts>();

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(db);

			conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			pstmt1.setInt(1, orderno);
			rs1 = pstmt1.executeQuery();

			while (rs1.next()) {
				ReorderParts rp = new ReorderParts();

				rp.setPartno(rs1.getString("partno"));
				rp.setCount(rs1.getInt("quantity"));
				lqs.add(rp);
			}

			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} finally {
			if (pstmt1 != null) {
				try {
					pstmt1.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}

		return lqs;
	}

}
