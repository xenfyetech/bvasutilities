package com.bestvalue.routine;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bestvalue.dto.PartsUnitsinStock;
import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class TodaysOrdersAdjust {
	static String dbFrom = "CH";

	private static List<PartsUnitsinStock> getTradedQuantities(String dbfrom) {
		String sql = "SELECT p.masterpartno,p.quantity FROM analytics.todaysorder p ORDER BY p.partnumber";

		System.out.println(sql);

		Connection conn = null;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		List<PartsUnitsinStock> lqs = new ArrayList<PartsUnitsinStock>();

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(dbfrom);

			conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery();

			while (rs1.next()) {
				PartsUnitsinStock sQuantity = new PartsUnitsinStock();
				sQuantity.setQuantity(rs1.getInt("quantity"));
				sQuantity.setPartno(rs1.getString("masterpartno").trim());
				lqs.add(sQuantity);
			}

			System.out.println(lqs.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} finally {
			if (pstmt1 != null) {
				try {
					pstmt1.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}

		return lqs;
	}

	public static void main(String[] args) {
		System.out.println("Processing..................");
		Connection connection = null;
		PreparedStatement preparedStatementUpdate = null;
		String updtInvoiceSql2 = "UPDATE bvasdb.parts  SET " + " UnitsInStock = UnitsInStock - ? WHERE interchangeno=?";

		List<PartsUnitsinStock> lqs = getTradedQuantities(dbFrom);

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(dbFrom);
			connection = bvasConFactory.getConnection();
			connection.setAutoCommit(false);

			for (PartsUnitsinStock partsstock : lqs) {

				preparedStatementUpdate = connection.prepareStatement(updtInvoiceSql2);
				preparedStatementUpdate.setInt(1, partsstock.getQuantity());
				preparedStatementUpdate.setString(2, partsstock.getPartno());
				System.out.println(preparedStatementUpdate.getWarnings());
				preparedStatementUpdate.executeUpdate();

			}
			connection.commit();
			System.out.println("ending..................");
		} catch (SQLException e) {
			System.out.println(e.toString());

			try {
				connection.rollback();
			} catch (SQLException e1) {
				System.out.println(e.toString());
			}
		} finally {
			if (preparedStatementUpdate != null) {
				try {
					preparedStatementUpdate.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}

	}
}
