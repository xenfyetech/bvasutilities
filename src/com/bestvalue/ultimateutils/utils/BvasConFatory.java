package com.bestvalue.ultimateutils.utils;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BvasConFatory {
	private static final String MP_DB = "jdbc:mysql://192.168.3.244:3306/mpsdb?zeroDateTimeBehavior=convertToNull&autoReconnect=true&characterEncoding=UTF-8&characterSetResults=UTF-8&user=root&password=001FS740";
	private static final String CH_DB = "jdbc:mysql://192.168.3.244:3306/bvasdb?zeroDateTimeBehavior=convertToNull&autoReconnect=true&characterEncoding=UTF-8&characterSetResults=UTF-8&user=BVAPPUSR&password=001FS740";
	private static final String GR_DB = "jdbc:mysql://192.168.2.244:3306/grsdb?zeroDateTimeBehavior=convertToNull&autoReconnect=true&characterEncoding=UTF-8&characterSetResults=UTF-8&user=BVAPPUSR&password=001FS740";

	private static final String LOCAL_CH = "jdbc:mysql://localhost:3306/bvasdb?zeroDateTimeBehavior=convertToNull&autoReconnect=true&characterEncoding=UTF-8&characterSetResults=UTF-8&user=root&password=001FS740";
	private static final String LOCAL_MP = "jdbc:mysql://localhost:3306/mpsdb?zeroDateTimeBehavior=convertToNull&autoReconnect=true&characterEncoding=UTF-8&characterSetResults=UTF-8&user=root&password=001FS740";
	private static final String LOCAL_GR = "jdbc:mysql://localhost:3306/grsdb?zeroDateTimeBehavior=convertToNull&autoReconnect=true&characterEncoding=UTF-8&characterSetResults=UTF-8&user=root&password=001FS740";
	private String jdbcDriver;
	private String mysqlUrl;

	public BvasConFatory(String dbpointer) {
		if (dbpointer.equalsIgnoreCase("CH")) {
			this.jdbcDriver = "com.mysql.jdbc.Driver";
			this.mysqlUrl = CH_DB;
		} else if (dbpointer.equalsIgnoreCase("MP")) {
			this.jdbcDriver = "com.mysql.jdbc.Driver";
			this.mysqlUrl = MP_DB;
		} else if (dbpointer.equalsIgnoreCase("GR")) {
			this.jdbcDriver = "com.mysql.jdbc.Driver";
			this.mysqlUrl = GR_DB;
		} else if (dbpointer.equalsIgnoreCase("localCH")) {
			this.jdbcDriver = "com.mysql.jdbc.Driver";
			this.mysqlUrl = LOCAL_CH;
		} else if (dbpointer.equalsIgnoreCase("localMP")) {
			this.jdbcDriver = "com.mysql.jdbc.Driver";
			this.mysqlUrl = LOCAL_MP;
		} else if (dbpointer.equalsIgnoreCase("localGR")) {
			this.jdbcDriver = "com.mysql.jdbc.Driver";
			this.mysqlUrl = LOCAL_GR;
		} else {
			this.mysqlUrl = "";
		}
	}

	public Connection getConnection() throws SQLException {
		Connection con = null;

		try {
			Class.forName(jdbcDriver).newInstance();
			con = DriverManager.getConnection(mysqlUrl);
		} catch (InstantiationException ex) {
			System.out.println("Exception---" + this.mysqlUrl + ex);
		} catch (IllegalAccessException ex) {
			System.out.println("Exception---" + this.mysqlUrl + ex);
		} catch (ClassNotFoundException ex) {
			System.out.println("Exception---" + this.mysqlUrl + ex);
		} catch (SQLException ex) {
			System.out.println("Exception---" + this.mysqlUrl + ex);
		}

		return con;
	}

	public String getJdbcDriver() {
		return jdbcDriver;
	}

	public String getMysqlUrl() {
		return mysqlUrl;
	}

	public void setJdbcDriver(String jdbcDriver) {
		this.jdbcDriver = jdbcDriver;
	}

	public void setMysqlUrl(String mysqlUrl) {
		this.mysqlUrl = mysqlUrl;
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
