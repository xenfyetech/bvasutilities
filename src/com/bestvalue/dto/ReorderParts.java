package com.bestvalue.dto;

public class ReorderParts {
	private Integer count;
	private String partno;

	public Integer getCount() {
		return count;
	}

	public String getPartno() {
		return partno;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public void setPartno(String partno) {
		this.partno = partno;
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
