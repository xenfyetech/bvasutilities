package com.bestvalue.dto;

public class PartLocation {
	private String location;
	private String partno;

	public String getLocation() {
		return location;
	}

	public String getPartno() {
		return partno;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setPartno(String partno) {
		this.partno = partno;
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
