package com.bestvalue.dto;

public class PartsPrice {
	public Float actualprice;
	public Float costprice;
	public Float listprice;
	public String partno;

	public Float getActualprice() {
		return actualprice;
	}

	public Float getCostprice() {
		return costprice;
	}

	public Float getListprice() {
		return listprice;
	}

	public String getPartno() {
		return partno;
	}

	public void setActualprice(Float actualprice) {
		this.actualprice = actualprice;
	}

	public void setCostprice(Float costprice) {
		this.costprice = costprice;
	}

	public void setListprice(Float listprice) {
		this.listprice = listprice;
	}

	public void setPartno(String partno) {
		this.partno = partno;
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
