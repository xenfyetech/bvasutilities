package com.bestvalue.dto;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;

public class Parts implements Serializable {
	private static final long serialVersionUID = 1L;
	private Float actualprice;
	private String capa;
	private String category;
	private Float costprice;
	private String dpinumber;
	private String interchangeno;
	private String keystonenumber;
	private Float listprice;
	private String location;
	private String makemodelcode;
	private String makemodelname;
	private String manufacturername;
	private String masterpartno;
	private String oemnumber;

	private Integer orderno;
	private String ordertype;
	private String othersidepartno;
	private String partdescription;
	private String partno;
	private String refpartno;
	private Integer reorderlevel;
	private String subcategory;
	private Integer supplierid;
	private Integer unitsinstock;
	private Integer unitsonorder;
	private String year;
	private Integer yearfrom;
	private Integer yearto;

	public Float getActualprice() {
		return actualprice;
	}

	public String getCapa() {
		return capa;
	}

	public String getCategory() {
		return category;
	}

	public Float getCostprice() {
		return costprice;
	}

	public String getDpinumber() {
		return dpinumber;
	}

	public String getInterchangeno() {
		return interchangeno;
	}

	public String getKeystonenumber() {
		return keystonenumber;
	}

	public Float getListprice() {
		return listprice;
	}

	public String getLocation() {
		return location;
	}

	public String getMakemodelcode() {
		return makemodelcode;
	}

	public String getMakemodelname() {
		return makemodelname;
	}

	public String getManufacturername() {
		return manufacturername;
	}

	public String getMasterpartno() {
		return masterpartno;
	}

	public String getOemnumber() {
		return oemnumber;
	}

	public Integer getOrderno() {
		return orderno;
	}

	public String getOrdertype() {
		return ordertype;
	}

	public String getOthersidepartno() {
		return othersidepartno;
	}

	public String getPartdescription() {
		return partdescription;
	}

	public String getPartno() {
		return partno;
	}

	public String getRefpartno() {
		return refpartno;
	}

	public Integer getReorderlevel() {
		return reorderlevel;
	}

	public String getSubcategory() {
		return subcategory;
	}

	public Integer getSupplierid() {
		return supplierid;
	}

	public Integer getUnitsinstock() {
		return unitsinstock;
	}

	public Integer getUnitsonorder() {
		return unitsonorder;
	}

	public String getYear() {
		return year;
	}

	public Integer getYearfrom() {
		return yearfrom;
	}

	public Integer getYearto() {
		return yearto;
	}

	public void setActualprice(Float actualprice) {
		this.actualprice = actualprice;
	}

	public void setCapa(String capa) {
		this.capa = capa;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public void setCostprice(Float costprice) {
		this.costprice = costprice;
	}

	public void setDpinumber(String dpinumber) {
		this.dpinumber = dpinumber;
	}

	public void setInterchangeno(String interchangeno) {
		this.interchangeno = interchangeno;
	}

	public void setKeystonenumber(String keystonenumber) {
		this.keystonenumber = keystonenumber;
	}

	public void setListprice(Float listprice) {
		this.listprice = listprice;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setMakemodelcode(String makemodelcode) {
		this.makemodelcode = makemodelcode;
	}

	public void setMakemodelname(String makemodelname) {
		this.makemodelname = makemodelname;
	}

	public void setManufacturername(String manufacturername) {
		this.manufacturername = manufacturername;
	}

	public void setMasterpartno(String masterpartno) {
		this.masterpartno = masterpartno;
	}

	public void setOemnumber(String oemnumber) {
		this.oemnumber = oemnumber;
	}

	public void setOrderno(Integer orderno) {
		this.orderno = orderno;
	}

	public void setOrdertype(String ordertype) {
		this.ordertype = ordertype;
	}

	public void setOthersidepartno(String othersidepartno) {
		this.othersidepartno = othersidepartno;
	}

	public void setPartdescription(String partdescription) {
		this.partdescription = partdescription;
	}

	public void setPartno(String partno) {
		this.partno = partno;
	}

	public void setRefpartno(String refpartno) {
		this.refpartno = refpartno;
	}

	public void setReorderlevel(Integer reorderlevel) {
		this.reorderlevel = reorderlevel;
	}

	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}

	public void setSupplierid(Integer supplierid) {
		this.supplierid = supplierid;
	}

	public void setUnitsinstock(Integer unitsinstock) {
		this.unitsinstock = unitsinstock;
	}

	public void setUnitsonorder(Integer unitsonorder) {
		this.unitsonorder = unitsonorder;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public void setYearfrom(Integer yearfrom) {
		this.yearfrom = yearfrom;
	}

	public void setYearto(Integer yearto) {
		this.yearto = yearto;
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
