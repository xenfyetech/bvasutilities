package com.bestvalue.dto;

public class SubtractQuantity {
	private String interchangeno;
	private Integer noorder;
	private Integer orderno;
	private String partno;
	private Float price;
	private Integer quantity;
	private String vendorpartno;

	public String getInterchangeno() {
		return interchangeno;
	}

	public Integer getNoorder() {
		return noorder;
	}

	public Integer getOrderno() {
		return orderno;
	}

	public String getPartno() {
		return partno;
	}

	public Float getPrice() {
		return price;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public String getVendorpartno() {
		return vendorpartno;
	}

	public void setInterchangeno(String interchangeno) {
		this.interchangeno = interchangeno;
	}

	public void setNoorder(Integer noorder) {
		this.noorder = noorder;
	}

	public void setOrderno(Integer orderno) {
		this.orderno = orderno;
	}

	public void setPartno(String partno) {
		this.partno = partno;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public void setVendorpartno(String vendorpartno) {
		this.vendorpartno = vendorpartno;
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
