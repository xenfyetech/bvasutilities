package com.bestvalue.dto;

public class PartDescription {
	private String partdescription;
	private String partno;

	public String getPartdescription() {
		return partdescription;
	}

	public void setPartdescription(String partdescription) {
		this.partdescription = partdescription;
	}

	public String getPartno() {
		return partno;
	}

	public void setPartno(String partno) {
		this.partno = partno;
	}

}
