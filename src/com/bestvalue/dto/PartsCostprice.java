package com.bestvalue.dto;

public class PartsCostprice {
	public Float costprice;
	public String partno;

	public Float getCostprice() {
		return costprice;
	}

	public String getPartno() {
		return partno;
	}

	public void setCostprice(Float costprice) {
		this.costprice = costprice;
	}

	public void setPartno(String partno) {
		this.partno = partno;
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
