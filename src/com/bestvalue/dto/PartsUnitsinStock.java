package com.bestvalue.dto;

public class PartsUnitsinStock {
	public String partno;
	public Integer quantity;

	public String getPartno() {
		return partno;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setPartno(String partno) {
		this.partno = partno;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
}
