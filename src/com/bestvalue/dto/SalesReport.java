package com.bestvalue.dto;

public class SalesReport {

	public Double discount;
	public Double grosstotal;
	public Double nettotal;
	public String orderdate;
	public Double tax;

	public Double getDiscount() {
		return discount;
	}

	public Double getGrosstotal() {
		return grosstotal;
	}

	public Double getNettotal() {
		return nettotal;
	}

	public String getOrderdate() {
		return orderdate;
	}

	public Double getTax() {
		return tax;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public void setGrosstotal(Double grosstotal) {
		this.grosstotal = grosstotal;
	}

	public void setNettotal(Double nettotal) {
		this.nettotal = nettotal;
	}

	public void setOrderdate(String orderdate) {
		this.orderdate = orderdate;
	}

	public void setTax(Double tax) {
		this.tax = tax;
	}

}
