package com.bestvalue.dto;

public class PartYear {
	public String partno;
	public String year;
	public String partdescription;
	public Integer yearfrom;
	public Integer yearto;

	public String getPartno() {
		return partno;
	}

	public void setPartno(String partno) {
		this.partno = partno;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getPartdescription() {
		return partdescription;
	}

	public void setPartdescription(String partdescription) {
		this.partdescription = partdescription;
	}

	public Integer getYearfrom() {
		return yearfrom;
	}

	public void setYearfrom(Integer yearfrom) {
		this.yearfrom = yearfrom;
	}

	public Integer getYearto() {
		return yearto;
	}

	public void setYearto(Integer yearto) {
		this.yearto = yearto;
	}

}

// ~ Formatted by Jindent --- http://www.jindent.com
