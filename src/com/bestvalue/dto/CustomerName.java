package com.bestvalue.dto;

public class CustomerName {

	public String customerid;

	public String customername;

	public String getCustomerid() {
		return customerid;
	}

	public String getCustomername() {
		return customername;
	}

	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}

	public void setCustomername(String customername) {
		this.customername = customername;
	}

}
