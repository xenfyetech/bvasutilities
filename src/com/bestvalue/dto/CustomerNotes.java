package com.bestvalue.dto;

public class CustomerNotes {
	String customerid;
	String customernotes;

	public String getCustomerid() {
		return customerid;
	}

	public String getCustomernotes() {
		return customernotes;
	}

	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}

	public void setCustomernotes(String customernotes) {
		this.customernotes = customernotes;
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
