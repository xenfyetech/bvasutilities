package com.bestvalue.dto;

import java.math.BigDecimal;
import java.util.Date;

public class PaidPaymentsData {

	public BigDecimal amount;
	public String companyname;
	public Date orderdate;
	public Integer orderno;
	public Date paymentdate;
	public Date shippeddate;

	public BigDecimal getAmount() {
		return amount;
	}

	public String getCompanyname() {
		return companyname;
	}

	public Date getOrderdate() {
		return orderdate;
	}

	public Integer getOrderno() {
		return orderno;
	}

	public Date getPaymentdate() {
		return paymentdate;
	}

	public Date getShippeddate() {
		return shippeddate;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}

	public void setOrderdate(Date orderdate) {
		this.orderdate = orderdate;
	}

	public void setOrderno(Integer orderno) {
		this.orderno = orderno;
	}

	public void setPaymentdate(Date paymentdate) {
		this.paymentdate = paymentdate;
	}

	public void setShippeddate(Date shippeddate) {
		this.shippeddate = shippeddate;
	}

}
