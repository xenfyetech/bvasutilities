package com.bestvalue.dto;

public class InvoiceDetailsOurPrice {
	private Integer invoicenumber;
	private Double ourprice;
	private String partno;

	public Integer getInvoicenumber() {
		return invoicenumber;
	}

	public Double getOurprice() {
		return ourprice;
	}

	public String getPartno() {
		return partno;
	}

	public void setInvoicenumber(Integer invoicenumber) {
		this.invoicenumber = invoicenumber;
	}

	public void setOurprice(Double ourprice) {
		this.ourprice = ourprice;
	}

	public void setPartno(String partno) {
		this.partno = partno;
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
