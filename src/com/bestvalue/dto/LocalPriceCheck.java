package com.bestvalue.dto;

public class LocalPriceCheck {
	public String partno;
	public Integer quantity;
	public Float sellingprice;
	public Integer supplierid;
	public String vendorpartno;

	public String getPartno() {
		return partno;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public Float getSellingprice() {
		return sellingprice;
	}

	public Integer getSupplierid() {
		return supplierid;
	}

	public String getVendorpartno() {
		return vendorpartno;
	}

	public void setPartno(String partno) {
		this.partno = partno;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public void setSellingprice(Float sellingprice) {
		this.sellingprice = sellingprice;
	}

	public void setSupplierid(Integer supplierid) {
		this.supplierid = supplierid;
	}

	public void setVendorpartno(String vendorpartno) {
		this.vendorpartno = vendorpartno;
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
