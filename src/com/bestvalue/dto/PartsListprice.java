package com.bestvalue.dto;

public class PartsListprice {
	public Float listprice;
	public String partno;

	public Float getListprice() {
		return listprice;
	}

	public String getPartno() {
		return partno;
	}

	public void setListprice(Float listprice) {
		this.listprice = listprice;
	}

	public void setPartno(String partno) {
		this.partno = partno;
	}
}
