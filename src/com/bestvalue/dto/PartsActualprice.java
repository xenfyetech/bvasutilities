package com.bestvalue.dto;

public class PartsActualprice {
	public Float actualprice;
	public String partno;

	public Float getActualprice() {
		return actualprice;
	}

	public String getPartno() {
		return partno;
	}

	public void setActualprice(Float actualprice) {
		this.actualprice = actualprice;
	}

	public void setPartno(String partno) {
		this.partno = partno;
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
