package com.bestvalue.dto;

public class StockContainer {
	public Integer containerquantity;
	public String partno;
	public Integer required;
	public Integer unitsinstock;

	public Integer getContainerquantity() {
		return containerquantity;
	}

	public String getPartno() {
		return partno;
	}

	public Integer getRequired() {
		return required;
	}

	public Integer getUnitsinstock() {
		return unitsinstock;
	}

	public void setContainerquantity(Integer containerquantity) {
		this.containerquantity = containerquantity;
	}

	public void setPartno(String partno) {
		this.partno = partno;
	}

	public void setRequired(Integer required) {
		this.required = required;
	}

	public void setUnitsinstock(Integer unitsinstock) {
		this.unitsinstock = unitsinstock;
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
