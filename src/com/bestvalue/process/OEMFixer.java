package com.bestvalue.process;

//~--- JDK imports ------------------------------------------------------------

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

//~--- non-JDK imports --------------------------------------------------------

import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class OEMFixer {
	private static Map<String, String> getMainPart(String dbfrom) {
		Map<String, String> lqs = new HashMap<String, String>();
		String sql = "SELECT partno, oemnumber  FROM parts WHERE oemnumber <> ''";
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(dbfrom);
			java.sql.Connection conn = bvasConFactory.getConnection();

			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery(sql);

			while (rs1.next()) {
				lqs.put(rs1.getString(1), rs1.getString(2));
			}

			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

		return lqs;
	}

	private static Map<String, String> getOemPart(String dbfrom) {
		Map<String, String> lqs = new HashMap<String, String>();
		String sql = "SELECT partno, oem  FROM ploemfix WHERE partno <> '' AND oem <> '' ";
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(dbfrom);
			java.sql.Connection conn = bvasConFactory.getConnection();

			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery(sql);

			while (rs1.next()) {
				lqs.put(rs1.getString(1), rs1.getString(2));
			}

			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

		return lqs;
	}

	private static Map<String, String> getVendoritemsOEM(String dbfrom) {
		HashSet<String> set = new HashSet<String>();
		Map<String, String> lqs = new HashMap<String, String>();
		String sql = "SELECT PARTNO , OEMNO , COUNT(OEMNO) FROM VENDORITEMS WHERE PARTNO <> '' AND OEMNO <> '' GROUP BY PARTNO,OEMNO  ORDER BY PARTNO, COUNT(OEMNO) DESC";
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(dbfrom);
			java.sql.Connection conn = bvasConFactory.getConnection();

			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery(sql);

			while (rs1.next()) {
				if (set.add(rs1.getString(1))) {
					lqs.put(rs1.getString(1), rs1.getString(2));
				}
			}

			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

		return lqs;
	}

	public static void main(String[] args) {

		/* getting the list of parts for the order */
		String dbfrom = "AM";
		Map<String, String> lqs = getOemPart(dbfrom);

		updateOem(lqs, dbfrom);

		Map<String, String> lqs2 = getVendoritemsOEM(dbfrom);

		updateOem(lqs2, dbfrom);

		Map<String, String> lqs3 = getMainPart(dbfrom);

		updateOem(lqs3, "NY");
		System.out.println("------finale------");
	}

	private static void updateOem(Map<String, String> lqs, String db) {
		Iterator<Entry<String, String>> iter = lqs.entrySet().iterator();
		BvasConFatory bvasConFactory = new BvasConFatory(db);
		PreparedStatement pstmt2 = null;

		try {
			java.sql.Connection conn = bvasConFactory.getConnection();

			while (iter.hasNext()) {
				Entry<String, String> mEntry = iter.next();

				System.out.println(mEntry.getKey() + " : " + mEntry.getValue());

				String updtInvoiceSql = "UPDATE parts  SET " + " oemnumber ='" + mEntry.getValue().replace("'", "")
						+ "'" + " WHERE partno='" + mEntry.getKey() + "' and oemnumber = ''";

				System.out.println(updtInvoiceSql);
				pstmt2 = conn.prepareStatement(updtInvoiceSql);
				pstmt2.executeUpdate(updtInvoiceSql);
			}

			System.out.println("END");
			pstmt2.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
