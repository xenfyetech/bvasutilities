package com.bestvalue.process;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class UpdateVendorPricesToTable {

	static String dbtarget = "CH";
	static String sql1 = "SELECT partno from analytics.giving";

	static String sql2 = "SELECT partno,SELLINGRATE,supplierid FROM vendoritems WHERE partno = ? AND SELLINGRATE > 0.01 ORDER BY SELLINGRATE LIMIT 1";

	public static void main(String[] args) throws ParseException, IOException {
		System.out.println("Processing..................");
		int i = 0;

		Connection connection = null;
		PreparedStatement customersStatement1 = null;
		PreparedStatement customersStatement2 = null;
		BvasConFatory bvasConFactory = new BvasConFatory(dbtarget);
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		StringBuffer sbf = new StringBuffer();
		try {

			connection = bvasConFactory.getConnection();
			customersStatement1 = connection.prepareStatement(sql1);
			rs1 = customersStatement1.executeQuery();

			while (rs1.next()) {

				// System.out.println(rs1.getString("partno"));

				i = 0;
				customersStatement2 = connection.prepareStatement(sql2);
				customersStatement2.setString(1, rs1.getString("partno"));
				rs2 = customersStatement2.executeQuery();

				while (rs2.next()) {
					System.out.println(rs2.getString("partno") + "," + rs2.getString("sellingrate") + ","
							+ rs2.getString("supplierid"));
				}
				if (rs2 != null) {
					try {
						rs2.close();
					} catch (SQLException e) {
						System.out.println(e.toString());
					}
				}

				if (customersStatement2 != null) {
					try {
						customersStatement2.close();
					} catch (SQLException e) {
						System.out.println(e.toString());
					}
				}

			}

			System.out.println("Content of StringBuffer written to File.");

		} catch (SQLException e) {
			System.out.println(e.toString());

			try {
				connection.rollback();
			} catch (SQLException e1) {
				System.out.println(e.toString());
			}
		} finally {
			if (rs1 != null) {
				try {
					rs1.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (customersStatement1 != null) {
				try {
					customersStatement1.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}
	}

}
