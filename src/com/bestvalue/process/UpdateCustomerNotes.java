package com.bestvalue.process;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//~--- non-JDK imports --------------------------------------------------------

import com.bestvalue.dto.CustomerNotes;
import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class UpdateCustomerNotes {
	private static List<CustomerNotes> getQualifyingParts(String dbfrom) {
		String sql = "SELECT id,addr2 from address where (addr2 is not null and addr2 <> '') ";

		System.out.println(sql);

		Connection conn = null;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		List<CustomerNotes> lqs = new ArrayList<CustomerNotes>();

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(dbfrom);

			conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery();

			while (rs1.next()) {
				CustomerNotes cn = new CustomerNotes();

				cn.setCustomerid(rs1.getString("id"));
				cn.setCustomernotes(rs1.getString("addr2"));
				lqs.add(cn);
			}

			System.out.println(lqs.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} finally {
			if (pstmt1 != null) {
				try {
					pstmt1.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}

		return lqs;
	}

	public static void main(String[] args) {
		String baseDB = "GR";
		Connection conn = null;
		PreparedStatement pstmt2 = null;
		BvasConFatory bvasConFactory = new BvasConFatory(baseDB);

		try {
			System.out.println("Processing..................");

			List<CustomerNotes> lqs = getQualifyingParts(baseDB);

			conn = bvasConFactory.getConnection();

			for (CustomerNotes customernotes : lqs) {
				String updateSQl = "insert into customernotes (customerid, customernotes) VALUES (?,?)";

				System.out.println(updateSQl);
				pstmt2 = conn.prepareStatement(updateSQl);
				pstmt2.setString(1, customernotes.getCustomerid());
				pstmt2.setString(2, customernotes.getCustomernotes());
				pstmt2.executeUpdate();
			}

			System.out.println("end");
			pstmt2.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
