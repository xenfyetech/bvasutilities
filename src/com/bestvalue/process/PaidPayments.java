package com.bestvalue.process;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.bestvalue.dto.PaidPaymentsData;
import com.bestvalue.ultimateutils.utils.BvasConFatory;
import com.bestvalue.ultimateutils.utils.DateUtils;

public class PaidPayments {

	public static void main(String[] args) {
		List<PaidPaymentsData> paidpaymentlist = new LinkedList<PaidPaymentsData>();

		BigDecimal totPayments = new BigDecimal("0");
		String targetDB = "GR";
		String sql = "SELECT a.OrderNo, b.CompanyName, a.OrderDate, a.DeliveredDate, (a.OrderTotal + a.Discount + a.StickerCharges) as TotalOrder, a.PaymentDate FROM VendorOrder a, Vendors b WHERE a.IsFinal='Y' AND a.SupplierId=b.SupplierId AND ( a.PaymentDate >='2015-01-01'  AND  a.PaymentDate <='2015-12-31')  AND A.SUPPLIERID <> 24  Order By a.PaymentDate ";
		PreparedStatement pstmt1 = null;
		Integer counter = 0;
		ResultSet rs1 = null;
		try {
			File returns = new File("c:/codestudio/bvasprocess/paidpayments/paidpayments_GR_2015_lcl.txt");
			FileWriter wrt = new FileWriter(returns);
			wrt.write("OrderNo" + "\t" + "CompanyName" + "\t" + "OrderDate" + "\t" + "OrderTotal" + "\t"
					+ "DeliveredDate" + "\t" + "PaymentDate" + "\t");
			BvasConFatory bvasConFactory = new BvasConFatory(targetDB);
			java.sql.Connection conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			// pstmt1.setDate(0, java.sql.Date(today.getTime()));
			// pstmt1.setDate(1, todate);
			rs1 = pstmt1.executeQuery(sql);
			while (rs1.next()) {

				Integer orderNo = rs1.getInt("OrderNo");
				String companyName = rs1.getString("CompanyName");
				String orderDate = DateUtils.convertMySQLToUSFormat(rs1.getString("OrderDate"));
				String deliveredDate = DateUtils.convertMySQLToUSFormat(rs1.getString("DeliveredDate"));
				double orderTotal = rs1.getDouble("TotalOrder");
				String paymentDate = rs1.getString("PaymentDate");

				if (paymentDate == null || paymentDate.trim().equals("") || paymentDate.trim().equals("0000-00-00")
						|| paymentDate.trim().equals("11-30-0002")) {
					continue;
				} else {
					PaidPaymentsData paidpayment = new PaidPaymentsData();
					paymentDate = DateUtils.convertMySQLToUSFormat(paymentDate);
					totPayments = totPayments.add(new BigDecimal(orderTotal).setScale(2, BigDecimal.ROUND_HALF_EVEN));
					wrt.write("\n");
					wrt.write(orderNo + "\t" + companyName + "\t" + orderDate + "\t" + orderTotal + "\t" + deliveredDate
							+ "\t" + paymentDate + "\t");

				}

			}
			System.out.println("END");
			wrt.close();
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

	}
}
