package com.bestvalue.process;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.Vector;

//~--- non-JDK imports --------------------------------------------------------

import com.bestvalue.ultimateutils.utils.BvasConFatory;
import com.bestvalue.ultimateutils.utils.DateUtils;
import com.bestvalue.ultimateutils.utils.NumberUtils;

public class VendorTransfer {
	public static void main(String[] args) {

		showLocalPurchases("01-01-2016", "12-31-2016", "3");
		/*
		 * showLocalPurchases("02-01-2015", "02-28-2015", "39");
		 * showLocalPurchases("03-01-2015", "03-31-2015", "39");
		 * showLocalPurchases("04-01-2015", "04-30-2015", "39");
		 * showLocalPurchases("05-01-2015", "05-31-2015", "39");
		 * showLocalPurchases("06-01-2015", "06-30-2015", "39");
		 * showLocalPurchases("07-01-2015", "07-31-2015", "39");
		 * showLocalPurchases("08-01-2015", "08-31-2015", "39");
		 * showLocalPurchases("09-01-2015", "09-30-2015", "39");
		 * showLocalPurchases("10-01-2015", "10-31-2015", "39");
		 * showLocalPurchases("11-01-2015", "11-30-2015", "39");
		 * showLocalPurchases("12-01-2015", "12-31-2015", "39");
		 */

	}

	public static void showLocalPurchases(String fromDate, String toDate, String vendId) {
		double grandamount = 0.0;
		String fileName = "C://codestudio//bvasprocess//reports//" + vendId + ".html";
		String mainHeading = "";
		Vector subHeadings = new Vector();
		Vector<Hashtable> data = new Vector<Hashtable>();
		String[][] totals = new String[3][2];

		System.out.println("___________________________________________________________________________");
		System.out.println(vendId + "     " + fromDate + "     " + toDate);

		try {
			BvasConFatory bvasConFactory = new BvasConFatory("CH");
			Connection connection = bvasConFactory.getConnection();
			Statement stmt = connection.createStatement();
			String sql = "SELECT DateEntered, PartNo, InvoiceNo, VendorPartNo, VendorInvNo, VendorInvDate, Quantity, Price, (Quantity * Price) as Amount From LocalOrders WHERE SupplierId="
					+ vendId + " and ";

			if (fromDate.trim().equals(toDate.trim())) {
				sql += " VendorInvDate='" + DateUtils.convertUSToMySQLFormat(toDate.trim()) + "'";
			} else {
				sql += " VendorInvDate>='" + DateUtils.convertUSToMySQLFormat(fromDate.trim()) + "' AND DateEntered<='"
						+ DateUtils.convertUSToMySQLFormat(toDate.trim()) + "'";
			}

			sql += " AND QUANTITY < 0";
			// sql += " AND InvoiceNo IN (SELECT orderno FROM branchtransfer
			// WHERE TRANSFERCODE = 'CHGR') ";

			System.out.println(sql);
			ResultSet rs = stmt.executeQuery(sql);
			int totQuantity = 0;
			int totItems = 0;
			double totAmount = 0.0;

			subHeadings.addElement("Date");
			subHeadings.addElement("Part No");
			subHeadings.addElement("Reason");
			subHeadings.addElement("Suppl Inv");
			subHeadings.addElement("Suppl Date");
			subHeadings.addElement("Suppl No");
			subHeadings.addElement("Quantity");
			subHeadings.addElement("Price");
			subHeadings.addElement("Amount");

			while (rs.next()) {
				String dateEntered = "";
				String partNo = "";
				String reason = "&nbsp;";
				int invNo = 0;
				String vendInvNo = "";
				String vendPartNo = "";
				String vendDate = "";
				int qty = 0;
				double price = 0.0;
				double amount = 0.0;

				dateEntered = DateUtils.convertMySQLToUSFormat(rs.getString("DateEntered"));
				partNo = rs.getString("PartNo");
				invNo = rs.getInt("InvoiceNo");
				vendInvNo = rs.getString("VendorInvNo");
				vendPartNo = rs.getString("VendorPartNo");
				vendDate = DateUtils.convertMySQLToUSFormat(rs.getString("VendorInvDate"));
				qty = rs.getInt("Quantity");
				price = rs.getDouble("Price");
				amount = rs.getDouble("Amount");

				if (invNo != 0) {
					reason = "For " + invNo;
				}

				totItems++;
				totQuantity += qty;
				totAmount += amount;

				Hashtable totData = new Hashtable();

				totData.put("Date", dateEntered);
				totData.put("Part No", partNo);
				totData.put("Reason", reason);
				totData.put("Suppl Inv", vendInvNo);
				totData.put("Suppl Date", vendDate);
				totData.put("Suppl No", vendPartNo);
				totData.put("Quantity", qty + "");
				totData.put("Price", price + "");
				totData.put("Amount", amount + "");
				data.addElement(totData);
			}

			String totAmountStr = totAmount + "";

			if (totAmountStr.indexOf(".") == totAmountStr.length() - 2) {
				totAmountStr += "0";
			}

			totAmountStr = NumberUtils.cutFractions(totAmountStr);
			totals[0][0] = "Total No. Of Items Purchased";
			// System.out.println(totals[0][0]);
			totals[0][1] = totItems + "";
			// System.out.println(totals[0][1]);
			totals[1][0] = "Total Quantity Purchased";
			// System.out.println(totals[1][0]);
			totals[1][1] = totQuantity + "";
			// System.out.println(totals[1][1]);
			totals[2][0] = "Total Purchase Amount";
			System.out.println(totals[2][0]);
			totals[2][1] = totAmountStr;
			System.out.println(totals[2][1]);
			rs.close();
			stmt.close();
			connection.close();
			System.out.println("End Game:");
		} catch (SQLException e) {

			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
