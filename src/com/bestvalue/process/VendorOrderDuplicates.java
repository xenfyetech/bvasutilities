package com.bestvalue.process;

//~--- JDK imports ------------------------------------------------------------

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

//~--- non-JDK imports --------------------------------------------------------

import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class VendorOrderDuplicates {
	private static List<Integer> getMainPart(String dbfrom) {
		List<Integer> lqs = new ArrayList<Integer>();
		String sql = "SELECT orderno  FROM vendororder";
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(dbfrom);
			java.sql.Connection conn = bvasConFactory.getConnection();

			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery(sql);

			while (rs1.next()) {
				lqs.add(rs1.getInt(1));
			}

			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

		return lqs;
	}

	public static void main(String[] args) {

		/* getting the list of parts for the order */
		String dbfrom = "CH";
		List<Integer> orders = getMainPart(dbfrom);
		HashSet<Integer> set = new HashSet<Integer>();

		for (Integer orderno : orders) {
			if (set.add(orderno)) {
			} else {
				System.out.println(orderno + ",");
			}
		}
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
