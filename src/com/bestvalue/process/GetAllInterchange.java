package com.bestvalue.process;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bestvalue.dto.Parts;
import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class GetAllInterchange {

	private static List<Parts> getParts(String dbfrom) {
		String sql = "SELECT * from bvasdb.parts where interchangeno = '' and subcategory in (1,2) order by dpinumber";

		System.out.println(sql);

		Connection conn = null;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		List<Parts> lqs = new ArrayList<Parts>();

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(dbfrom);

			conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery();

			while (rs1.next()) {
				Parts p = new Parts();

				p.setPartno(rs1.getString("partno"));
				p.setInterchangeno(rs1.getString("interchangeno"));
				p.setPartdescription(rs1.getString("partdescription"));
				p.setMakemodelcode(rs1.getString("makemodelcode"));
				p.setYear(rs1.getString("year"));
				p.setSubcategory(rs1.getString("subcategory"));
				p.setSupplierid(rs1.getInt("supplierid"));
				p.setOrderno(rs1.getInt("orderno"));
				p.setCostprice(rs1.getFloat("costprice"));
				p.setListprice(rs1.getFloat("listprice"));
				p.setActualprice(rs1.getFloat("actualprice"));
				p.setUnitsinstock(0);
				p.setUnitsonorder(0);
				p.setReorderlevel(0);

				p.setKeystonenumber(rs1.getString("keystonenumber"));
				p.setOemnumber(rs1.getString("oemnumber"));
				p.setLocation("");
				p.setOrdertype(rs1.getString("ordertype"));
				p.setCapa(rs1.getString("capa"));
				p.setRefpartno(rs1.getString("refpartno"));
				p.setCategory(rs1.getString("category"));
				p.setMasterpartno(rs1.getString("masterpartno"));
				p.setOthersidepartno(rs1.getString("othersidepartno"));
				p.setYearfrom(rs1.getInt("yearfrom"));
				p.setYearto(rs1.getInt("yearto"));
				p.setDpinumber(rs1.getString("dpinumber"));
				p.setMakemodelname(rs1.getString("makemodelname"));
				p.setManufacturername(rs1.getString("manufacturername"));
				lqs.add(p);
			}

			System.out.println(lqs.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} finally {
			if (pstmt1 != null) {
				try {
					pstmt1.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}

		return lqs;
	}

	public static void main(String[] args) {
		String sql = "SELECT * from  BVASDB.parts p  WHERE p.interchangeno = ? order by partno";
		String dbfrom = "CH";
		List<Parts> mainpartlist = getParts(dbfrom);

		try {
			File returns = new File("c:/codestudio/bvasprocess/analytics/dpi.csv");
			FileWriter wrt = new FileWriter(returns);
			BvasConFatory bvasConFactory = new BvasConFatory(dbfrom);
			PreparedStatement pstmt1 = null;
			ResultSet rs1 = null;
			java.sql.Connection conn = bvasConFactory.getConnection();
			for (Parts mainpart : mainpartlist) {
				wrt.write("\n");

				pstmt1 = conn.prepareStatement(sql);
				pstmt1.setString(1, mainpart.getPartno());
				rs1 = pstmt1.executeQuery();

				// System.out.println("PARTNO" + "," + "DPINUMBER" + "," +
				// "MAKE" + "," + "MODEL" + "," + "PARTDESCRIPTION" + "," +
				// "PARTLINK" + "," + "UNITSINSTOCK" );
				wrt.write("MAIN" + "," + mainpart.getPartno() + "," + mainpart.getDpinumber() + ","
						+ mainpart.getManufacturername() + "," + mainpart.getMakemodelname() + ","
						+ mainpart.getPartdescription() + "," + mainpart.getKeystonenumber() + ","
						+ mainpart.getUnitsinstock());

				while (rs1.next()) {

				}

				rs1.close();
				pstmt1.close();

			}
			System.out.println("***END****");
			wrt.close();
			conn.close();
		} catch (SQLException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

}
