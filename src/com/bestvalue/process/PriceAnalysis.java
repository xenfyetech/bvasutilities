package com.bestvalue.process;

//~--- JDK imports ------------------------------------------------------------

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//~--- non-JDK imports --------------------------------------------------------

import com.bestvalue.dto.PartsPrice;
import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class PriceAnalysis {
	private static List<PartsPrice> getOddOnes(String db, String subcategory) {
		String sql = "SELECT p.costprice,p.listprice, p.actualprice,p.partno from  parts p  WHERE p.costprice <= 0 and p.actualprice > 0";

		System.out.println(sql);

		Connection conn = null;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		List<PartsPrice> lqs = new ArrayList<PartsPrice>();

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(db);

			conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);

			// pstmt1.setString(1, subcategory);
			rs1 = pstmt1.executeQuery();

			while (rs1.next()) {
				PartsPrice sQuantity = new PartsPrice();

				sQuantity.setPartno(rs1.getString("PartNo"));
				sQuantity.setActualprice(rs1.getFloat("actualprice"));
				sQuantity.setCostprice(rs1.getFloat("costprice"));
				sQuantity.setListprice(rs1.getFloat("listprice"));
				System.out.println("'" + rs1.getString("PartNo") + "',");
				lqs.add(sQuantity);
			}

			System.out.println(lqs.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} finally {
			if (pstmt1 != null) {
				try {
					pstmt1.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}

		return lqs;
	}

	public static void main(String[] args) {
		System.out.println("Processing..................");

		String db = "CH";
		List<PartsPrice> lqs = getOddOnes(db, "99");

		for (PartsPrice pp : lqs) {
			BigDecimal factor = new BigDecimal(1.43f);
			BigDecimal actual = new BigDecimal(pp.getActualprice());
			BigDecimal cost = actual.multiply(factor);
			BigDecimal margin = cost.subtract(actual);
			BigDecimal hundred = new BigDecimal("100.00");
			BigDecimal total = margin.multiply(hundred);

			total = total.divide(cost, 2, RoundingMode.CEILING);

			// System.out.println(total);
		}

		// updateCostPrice(lqs, db);
		System.out.println("Finished Processing..................");
	}

	private static void updateCostPrice(List<PartsPrice> lqs, String db) {
		BvasConFatory bvasConFactory = new BvasConFatory(db);
		PreparedStatement pstmt2 = null;

		try {
			java.sql.Connection conn = bvasConFactory.getConnection();

			for (PartsPrice ps : lqs) {
				String updtInvoiceSql = "UPDATE parts  SET " + " costprice =1.43 * " + ps.getActualprice()
						+ ", listprice = 2.0 * actualprice  WHERE partno='" + ps.getPartno() + "'";

				System.out.println(updtInvoiceSql);
				pstmt2 = conn.prepareStatement(updtInvoiceSql);

				// pstmt2.executeUpdate(updtInvoiceSql);
			}

			System.out.println("END");
			pstmt2.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
