package com.bestvalue.process;

//~--- JDK imports ------------------------------------------------------------

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//~--- non-JDK imports --------------------------------------------------------

import com.bestvalue.dto.PartLocation;
import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class UpdateLocation {
	private static List<PartLocation> getPartPartdesc() {
		List<PartLocation> los = new ArrayList<PartLocation>();
		String sql = "SELECT partno, location FROM parts WHERE LOCATION <> ''";
		PreparedStatement pstmt1 = null;
		Integer counter = 0;
		ResultSet rs1 = null;

		try {
			BvasConFatory bvasConFactory = new BvasConFatory("localMP");
			java.sql.Connection conn = bvasConFactory.getConnection();

			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery(sql);

			while (rs1.next()) {
				counter++;

				PartLocation os = new PartLocation();

				os.setPartno(rs1.getString("partno"));
				os.setLocation(rs1.getString("location"));
				los.add(os);
			}

			System.out.println(counter);

			// System.out.println(partsMap.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

		return los;
	}

	public static void main(String[] args) {

		/* getting the list of parts for the order */
		List<PartLocation> los = getPartPartdesc();
		BvasConFatory bvasConFactory = new BvasConFatory("MP");
		PreparedStatement pstmt2 = null;

		try {
			java.sql.Connection conn = bvasConFactory.getConnection();

			for (PartLocation os : los) {
				String updtInvoiceSql = "UPDATE bvasdb.parts" + " SET location ='" + os.getLocation()
						+ "' WHERE (PARTNO='" + os.getPartno() + "') OR (INTERCHANGENO='" + os.getPartno() + "')";

				System.out.println(updtInvoiceSql);
				pstmt2 = conn.prepareStatement(updtInvoiceSql);
				pstmt2.executeUpdate(updtInvoiceSql);
			}

			pstmt2.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
