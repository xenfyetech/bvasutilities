package com.bestvalue.process;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class DeletePartsMonthlySales {

	public static void main(String[] args) {
		String baseDB = "GR";
		List<String> parts = getParts(baseDB);

		try {
			String deletepartssql = "delete from partsmonthlysales WHERE  partno = ?";
			BvasConFatory bvasConFactoryBase = new BvasConFatory(baseDB);

			Connection connBase;
			connBase = bvasConFactoryBase.getConnection();

			PreparedStatement statmt1Target = null;

			for (String partno : parts) {
				System.out.println(" processing for -> " + partno.trim());
				if (partno != null) {
					statmt1Target = connBase.prepareStatement(deletepartssql);
					statmt1Target.setString(1, partno.trim()); // set
					statmt1Target.executeUpdate();
					statmt1Target.close();
				}
			}

			connBase.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private static List<String> getParts(String db) {
		String sql = "SELECT partno FROM parts WHERE ordertype NOT IN ('T','W','R','L','E')";

		Connection conn = null;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		List<String> lqs = new ArrayList<String>();

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(db);

			conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery();

			while (rs1.next()) {
				lqs.add(rs1.getString("partno"));
			}

			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} finally {
			if (pstmt1 != null) {
				try {
					pstmt1.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}

		return lqs;
	}

}
