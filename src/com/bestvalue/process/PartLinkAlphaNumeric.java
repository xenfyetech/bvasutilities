package com.bestvalue.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class PartLinkAlphaNumeric {
	public static void main(String[] args) {
		String dbfrom = "MR";
		Map<String, String> lqs = getPLPart(dbfrom);
		sanitizeAlphanumeric(lqs, dbfrom);
	}

	private static void sanitizeAlphanumeric(Map<String, String> lqs, String db) {
		Iterator<Entry<String, String>> iter = lqs.entrySet().iterator();
		BvasConFatory bvasConFactory = new BvasConFatory(db);
		PreparedStatement pstmt2 = null;

		String regex = "^[a-zA-Z0-9]+$";

		Pattern pattern = Pattern.compile(regex);

		try {
			java.sql.Connection conn = bvasConFactory.getConnection();

			while (iter.hasNext()) {
				Entry<String, String> mEntry = iter.next();

				// System.out.println(mEntry.getKey() + " : " + mEntry.getValue());

				Matcher matcher = pattern.matcher(mEntry.getValue());

				if (matcher.matches()) {

				} else {
					String newkeystonenumber = mEntry.getValue().replaceAll("[^a-zA-Z0-9]", "").trim();
					System.out.println(mEntry.getKey() + "	" + newkeystonenumber);

					String updtInvoiceSql = "UPDATE parts  SET " + " KEYSTONENUMBER ='" + newkeystonenumber + "'"
							+ " WHERE partno='" + mEntry.getKey() + "';";

					// System.out.println(updtInvoiceSql);
					pstmt2 = conn.prepareStatement(updtInvoiceSql);
					pstmt2.executeUpdate(updtInvoiceSql);
				}

			}

			System.out.println("END");
			pstmt2.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
	}

	private static Map<String, String> getPLPart(String dbfrom) {
		Map<String, String> lqs = new HashMap<String, String>();
		String sql = "SELECT partno, KEYSTONENUMBER  FROM PARTS WHERE partno <> '' AND KEYSTONENUMBER <> '' ";
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(dbfrom);
			java.sql.Connection conn = bvasConFactory.getConnection();

			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery(sql);

			while (rs1.next()) {
				lqs.put(rs1.getString(1), rs1.getString(2));
			}

			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

		return lqs;
	}
}
