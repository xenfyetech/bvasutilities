package com.bestvalue.process;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//~--- non-JDK imports --------------------------------------------------------

import com.bestvalue.dto.ReorderParts;
import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class PartsRemoveList {
	public static Integer orderfrom = 14271;
	public static String orderlist = "60007,16432,17316";
	public static String[] orders = orderlist.split(",");

	private static List<ReorderParts> getParts(Integer orderno, String db) {
		String sql = "SELECT partno,quantity from vendorordereditems where orderno =" + orderno;

		System.out.println(sql);

		Connection conn = null;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		List<ReorderParts> lqs = new ArrayList<ReorderParts>();

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(db);

			conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery();

			while (rs1.next()) {
				ReorderParts rp = new ReorderParts();

				rp.setPartno(rs1.getString("partno"));
				rp.setCount(rs1.getInt("quantity"));
				lqs.add(rp);
			}

			System.out.println(lqs.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} finally {
			if (pstmt1 != null) {
				try {
					pstmt1.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}

		return lqs;
	}

	// 5666 � AM, CH60006, GR16431, NY17315
	// 5674 � AM14271, CH60007, GR16432, NY17316
	// 5683 � AM14272, CH60008, GR16433, NY17317
	public static void main(String[] args) {
		String sqlQuery = "UPDATE VENDORORDEREDITEMS SET QUANTITY = QUANTITY - ? WHERE ORDERNO= ? AND PARTNO = ?";
		Connection conn = null;
		PreparedStatement pstmt1 = null;
		BvasConFatory bvasConFactory = new BvasConFatory("CH");

		try {
			conn = bvasConFactory.getConnection();

			for (String order : orders) {
				List<ReorderParts> reparts = getParts(Integer.parseInt(order.trim()), "CH");

				for (ReorderParts rp : reparts) {
					System.out.println(rp.getPartno());
					pstmt1 = conn.prepareStatement(sqlQuery);
					pstmt1.setInt(1, rp.getCount());
					pstmt1.setInt(2, orderfrom);
					pstmt1.setString(3, rp.getPartno());
					pstmt1.execute();
				}
			}

			// rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
