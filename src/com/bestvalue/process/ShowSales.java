package com.bestvalue.process;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class ShowSales {

	public static void main(String[] args) {

		System.out.println("-----start of sales process-----");
		String mainSql = "SELECT OrderDate, SUM(InvoiceTotal), SUM(Discount), SUM(Tax) FROM Invoice  GROUP BY OrderDate";
		String targetDB = "CH";
		java.sql.Date orderdate = null;
		Double grosstotal = 0.00d;
		Double discount = 0.00d;
		Double tax = 0.00d;
		Double nettotal = 0.00d;
		Connection connectionMain = null;
		PreparedStatement pstmtMain = null;
		ResultSet rsMain = null;

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(targetDB);
			connectionMain = bvasConFactory.getConnection();

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

	}

}
