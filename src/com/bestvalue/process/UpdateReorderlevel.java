package com.bestvalue.process;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//~--- non-JDK imports --------------------------------------------------------

import com.bestvalue.dto.ReorderParts;
import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class UpdateReorderlevel {
	private static List<ReorderParts> getQualifyingParts(String dbfrom) {
		String sql = "SELECT partno, reorderlevel from parts where reorderlevel > 0";

		System.out.println(sql);

		Connection conn = null;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		List<ReorderParts> lqs = new ArrayList<ReorderParts>();

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(dbfrom);

			conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery();

			while (rs1.next()) {
				ReorderParts rp = new ReorderParts();

				rp.setCount(rs1.getInt("reorderlevel"));
				rp.setPartno(rs1.getString("partno"));
				lqs.add(rp);
			}

			System.out.println(lqs.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} finally {
			if (pstmt1 != null) {
				try {
					pstmt1.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}

		return lqs;
	}

	public static void main(String[] args) {
		String baseDB = "NY";
		String targetDB = "NY";
		Connection conn = null;
		PreparedStatement pstmt1 = null;
		BvasConFatory bvasConFactory = new BvasConFatory(targetDB);

		try {
			System.out.println("Processing..................");

			List<ReorderParts> lqs = getQualifyingParts(baseDB);

			conn = bvasConFactory.getConnection();

			for (ReorderParts rp : lqs) {
				String str1 = "UPDATE PARTS SET reorderlevel =" + rp.getCount() + " WHERE partno = '" + rp.getPartno()
						+ "';";

				pstmt1 = conn.prepareStatement(str1);
				pstmt1.executeUpdate();
			}

			pstmt1.close();
			conn.close();
			System.out.println("All done..................");
		} catch (SQLException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
