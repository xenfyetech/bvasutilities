package com.bestvalue.process;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//~--- non-JDK imports --------------------------------------------------------

import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class UpdateReorderType {
	private static List<String> getQualifyingParts(String dbfrom) {
		String sql = "SELECT partno from parts where ordertype not in ( 'D', 'X','S', 'W', 'R','E') ";

		System.out.println(sql);

		Connection conn = null;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		List<String> lqs = new ArrayList<String>();

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(dbfrom);

			conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery();

			while (rs1.next()) {
				lqs.add(rs1.getString("partno"));
			}

			System.out.println(lqs.size());
			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} finally {
			if (pstmt1 != null) {
				try {
					pstmt1.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}

		return lqs;
	}

	public static void main(String[] args) {
		String baseDB = "CH";
		String targetDB = "GR";
		Connection conn = null;
		PreparedStatement pstmt1 = null;
		PreparedStatement pstmt2 = null;
		ResultSet rs1 = null;
		BvasConFatory bvasConFactory = new BvasConFatory(targetDB);

		try {
			System.out.println("Processing..................");

			List<String> lqs = getQualifyingParts(baseDB);

			conn = bvasConFactory.getConnection();

			for (String partno : lqs) {
				String str1 = "SELECT vi.partno ,vi.supplierid,  vi.sellingrate,V.CompanyType FROM vendoritems vi , vendors v "
						+ "WHERE vi.SupplierID = v.SupplierID AND vi.sellingrate > 0   AND v.CompanyType != 'Z' "
						+ "AND vi.partno = '" + partno + "'" + " ORDER BY vi.sellingrate LIMIT 1";

				pstmt1 = conn.prepareStatement(str1);
				rs1 = pstmt1.executeQuery(str1);

				while (rs1.next()) {
					System.out
							.println(partno + ",    " + rs1.getInt("supplierid") + ",  " + rs1.getFloat("sellingrate"));

					String updateSQl = "update parts set compprice1 =" + rs1.getFloat("sellingrate") + ", supplierid="
							+ rs1.getInt("supplierid") + ", ordertype='" + rs1.getString("CompanyType") + "' "
							+ " WHERE partno = '" + partno + "' ";

					System.out.println(updateSQl);
					pstmt2 = conn.prepareStatement(updateSQl);
					pstmt2.executeUpdate();
				}
			}

			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
