package com.bestvalue.process;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

//~--- non-JDK imports --------------------------------------------------------

import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class CostOfGoods {
	public static String baseDB = "local";
	public static String orderdatefrom = "2014-10-01";
	public static String orderdateto = "2014-10-31";
	public static String sql = "SELECT i.InvoiceNumber, i.InvoiceTotal, i.Discount FROM Invoice i, InvoiceDetails id WHERE i.InvoiceNumber = id.InvoiceNumber ";

	public static void main(String[] args) {
		System.out.println("-------START-------");
		sql = sql + " AND OrderDate BETWEEN '" + orderdatefrom + "' AND  '" + orderdateto + "' ORDER BY 1";
		System.out.println(sql);

		Connection conn = null;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		BvasConFatory bvasConFactory = new BvasConFatory(baseDB);
		double totInvoiceTotal = 0.0;
		double totDiscount = 0.0;
		double totInvoicePrice = 0;
		double totInvoiceSoldPrice = 0;
		double totTotalPrice = 0;
		double totTotalSoldPrice = 0;
		double totMargin = 0;
		double totPercent = 0;
		int totItems = 0;

		try {
			conn = bvasConFactory.getConnection();
			pstmt1 = conn.prepareStatement(sql);
			rs1 = pstmt1.executeQuery(sql);

			while (rs1.next()) {
				System.out.println(rs1.getInt("InvoiceNumber"));

				int invoiceNo = rs1.getInt(1);
				double invoiceTotal = rs1.getDouble(2);
				double discount = rs1.getDouble(3);
				double invoicePrice = 0;
				double invoiceSoldPrice = 0;
				double margin = 0;
				String invoiceTotalStr = invoiceTotal + "";
				String discountStr = discount + "";

				if (invoiceTotalStr.indexOf(".") == invoiceTotalStr.length() - 2) {
					invoiceTotalStr += "0";
				}

				if (discountStr.indexOf(".") == discountStr.length() - 2) {
					discountStr += "0";
				}

				String invoicePriceStr = "";
				String invoiceSoldPriceStr = "";
				String marginStr = "";
				String remarks = "";
			}

			rs1.close();
			pstmt1.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}

		System.out.println("-------END-------");
	}
}

// ~ Formatted by Jindent --- http://www.jindent.com
