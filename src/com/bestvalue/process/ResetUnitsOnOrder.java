package com.bestvalue.process;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.bestvalue.ultimateutils.utils.BvasConFatory;

public class ResetUnitsOnOrder {

	public static void main(String[] args) {
		System.out.println("STARTING UNITS ON ORDER UPDATE PROCESS");

		String branchesstring = "CH";
		String[] branches = branchesstring.split(",");
		for (String targetDB : branches) {
			System.out.println("BRANCH : " + targetDB);
			System.out.println("_____________________________________________");
			targetDB = targetDB.trim();
			List<Integer> listoforders = getQualifyingOrders(targetDB);

			processOrders(listoforders, targetDB);

		}

		System.out.println("ENDING UNITS ON ORDER UPDATE PROCESS");

	}

	private static void processOrders(List<Integer> listoforders, String targetDB) {

		String orderSqlMain = "select partno, quantity from vendorordereditems where orderno=?";
		String updateSqlMain = "update parts set unitsonorder = unitsonorder + ? where partno = ?";
		Connection connection = null;
		PreparedStatement sqlOrderStmt = null;
		PreparedStatement sqlUpdateParts = null;
		ResultSet rsOrder = null;

		try {
			BvasConFatory bvasConFactory = new BvasConFatory(targetDB);
			connection = bvasConFactory.getConnection();

			for (Integer orderno : listoforders) {
				System.out.println("processing order ....." + orderno);
				sqlOrderStmt = connection.prepareStatement(orderSqlMain);
				sqlOrderStmt.setInt(1, orderno);
				rsOrder = sqlOrderStmt.executeQuery();

				while (rsOrder.next()) {
					sqlUpdateParts = connection.prepareStatement(updateSqlMain);
					sqlUpdateParts.setInt(1, rsOrder.getInt("quantity"));
					sqlUpdateParts.setString(2, rsOrder.getString("partno").trim());
					sqlUpdateParts.executeUpdate();
				}

			} // orderno
		} catch (SQLException e) {
			System.out.println(e.toString());
		} finally {

			if (rsOrder != null) {
				try {
					rsOrder.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
			if (sqlOrderStmt != null) {
				try {
					sqlOrderStmt.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (sqlUpdateParts != null) {
				try {
					sqlUpdateParts.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}

	}

	public static List<Integer> getQualifyingOrders(String targetDB) {

		List<Integer> listoforders = new LinkedList<Integer>();
		Connection connection = null;
		PreparedStatement stmtOrder = null;
		ResultSet rsOrder = null;

		try {
			String orderSQL = "SELECT distinct orderno from vendororder where isfinal = 'Y' AND UpdatedInventory= 'N'";
			BvasConFatory bvasConFactory = new BvasConFatory(targetDB);
			connection = bvasConFactory.getConnection();
			stmtOrder = connection.prepareStatement(orderSQL);
			rsOrder = stmtOrder.executeQuery();

			while (rsOrder.next()) {
				listoforders.add(rsOrder.getInt("orderno"));
			}
		} catch (SQLException e) {
			System.out.println(e.toString());
		} finally {

			if (rsOrder != null) {
				try {
					rsOrder.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
			if (stmtOrder != null) {
				try {
					stmtOrder.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					System.out.println(e.toString());
				}
			}
		}

		return listoforders;
	}

}
